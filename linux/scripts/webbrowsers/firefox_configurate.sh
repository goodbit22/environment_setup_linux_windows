#!/bin/bash
set -ue

# shellcheck source=/dev/null
if [ -f './function_help.sh' ]; then
	source './function_help.sh'
else
	echo -e "${RED}Nie ma wymaganego pliku function_help.sh${WHITE}"
	exit 1;
fi

display-sites(){
    sites=("https://www.youtube.com" "https://myanimelist.net/" "https://github.com/goodbit22" "https://gitlab.com/goodbit22"
	"https://mega.nz/fm/QgxmEarZ" "https://mail.google.com")
    for site in "${sites[@]}"
    do  
        echo -e "${BLUE}Trwa otwieranie strony $site ${WHITE}"
        firefox -new-tab "$site" &
    done
}

add_sites(){
	sites=('')
    	for site in "${sites[@]}"
    	do  
        	echo -e "${BLUE}Trwa otwieranie strony $site ${WHITE}"
        	firefox -new-window "$site" &
    	done

}
add_extensions(){
	local extensions=("https://addons.mozilla.org/firefox/downloads/file/3953362/adblocker_ultimate-latest.xpi" 
"https://addons.mozilla.org/firefox/downloads/file/3655554/disconnect-latest.xpi"
"https://addons.mozilla.org/firefox/downloads/file/3954503/darkreader-latest.xpi"
"https://addons.mozilla.org/firefox/downloads/file/3956871/duckduckgo_for_firefox-latest.xpi"
"https://addons.mozilla.org/firefox/downloads/file/3923300/facebook_container-latest.xpi"
"https://addons.mozilla.org/firefox/downloads/file/3872283/privacy_badger17-latest.xpi")
 	wget "${extensions[@]}" && firefox  -silent -install-global-extension "${extensions[@]##*/}"  && rm "${extensions[@]##*/}" 
}

main(){
	display-sites
	add_sites
	add_extensions
}
main

