#!/bin/bash
set -uo pipefail

import_profile_mozilla(){
  	sudo apt install -y gnupg || sudo dnf install -y gnupg 
    local path_profile_firefox="$HOME/.mozilla/"
	if [ -f  "$path_profile_firefox" ]; then
		if [ -f "firefox-browser-profile.tar.bz2.gpg" ];then
			echo -e "Trwa importowanie profilu firefox" 
			gpg "firefox-browser-profile.tar.bz2.gpg" && 
			cp "firefox-browser-profile.tar.bz2" "$path_profile_firefox" && 
			tar -xvf  "${path_profile_firefox}/firefox-browser-profile.tar.bz2" && 
			echo -e "Zaimportowano profil"
		fi
	fi 
}

import_profile_chrome(){
	sudo apt install -y gnupg || sudo dnf install -y gnupg 
	local path_profile_chrome="${HOME}/.config/"
	if [ -f  "$path_profile_chrome" ]; then
		if [ -f "google-chrome-profile.tar.bz2.gpg" ];then
		    echo -e "Trwa importowanie profilu chrome"
			gpg "google-chrome-profile.tar.bz2.gpg" && 
			cp "google-chrome-profile.tar.bz2" "$path_profile_chrome" && 
			tar -xvf  "google-chrome-profile.tar.bz2" && 
			echo -e "Zaimportowano profil"
		fi
	fi
}

export_profile_firefox(){
    sudo apt install -y gnupg || sudo dnf install -y gnupg 
	local path_profile_firefox="$HOME/.mozilla/"
	if [ -f  "$path_profile_firefox" ]; then
		echo -e "Trwa eksportowanie profilu firefox"
		tar -jcvf firefox-browser-profile.tar.bz2 "$path_profile_firefox"
		gpg -c firefox-browser-profile.tar.bz2 && echo -e "Wyeksportowano profil"
	fi
}

export_profile_chrome(){ 
    sudo apt install -y gnupg || sudo dnf install -y gnupg 
	local path_profile_chrome="${HOME}/.config/google-chrome"
	if [ -f  "$path_profile_chrome" ]; then
		echo -e "Trwa eksportowanie profilu firefox"
		tar -jcvf google-chrome-profile.tar.bz2 "$path_profile_chrome"
		gpg -c google-chrome-profile.tar.bz2 && echo -e "Wyeksportowano profil"
	fi
}

help()
{
   echo "Script for importing and exporting web browser (Chrome and Firefox) profiles"
   echo
   echo "Usage: ./profiles_webrowsers.sh [-i|e]"
   echo "options:"
   echo "-i  	Import profiles"
   echo "-e  	Export profiles"
   echo
}

while getopts ":ei" option; do 
	case $option in 
		e)
			[ "$1"  = '-e' ]  && (export_profile_firefox;  export_profile_chrome; exit 0 ) ;;
		i)
			[ "$1"  = '-i' ]  && ( import_profile_mozilla; import_profile_chrome; exit 0 ) ;;
		\?) help; exit 1
	esac
done