#!/bin/bash 
set -uo pipefail

# shellcheck source=/dev/null
if [ -f './function_help.sh' ]; then
	source './function_help.sh'
else
	echo -e "${RED}Nie ma wymaganego pliku function_help.sh${WHITE}"
	exit 1;
fi

main(){
  docker-compose_update
}

main