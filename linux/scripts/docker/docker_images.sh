#!/bin/bash
set -uo pipefail


pull_images(){
    local -r image_json='../../../images_docker.json'
    if [ ! -f "${image_json}" ]; then
        echo "Nie ma takiego pliku"
    else
        mapfile -t docker_images_name < <(jq -c '.[].name' "${image_json}")
        mapfile -t docker_images_tag < <(jq -c '.[].tag' "${image_json}")
        for index in ${!docker_images_name[*]}; do 
            docker_images["${index}"]=$(echo "${docker_images_name[$index]}:${docker_images_tag[$index]}" | tr -d '"' )
        done
        if [ "${#docker_images[@]}" != 0 ]; then
            for docker_img in "${docker_images[@]}";do
                if [[ "$docker_img" != ':' ]]; then
                    echo "Trwa sciaganie obrazu $docker_img"
                    docker pull "$docker_img" 
                fi 
        done
        else
            echo "Plik ${image_json} nie zawiera żadnych obrazów"
        fi
    fi
}
pull_images