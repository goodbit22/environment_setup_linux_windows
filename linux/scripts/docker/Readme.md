# 1)Opis skryptów

    docker_images.sh - skrypt do pobierania wielu obrazów docker
    docker_without_sudo.sh -  skrypt ktory umozliwia uruchamiania docker bez sudo 
    docker_compose_update.sh - skrypt do zaktualizowania docker-compose do najnowszej wersji

## 2)Uruchamianie skryptów

1.komendy:

* bash docker_images.sh
* bash docker_without_sudo.sh
* bash docker_compose_update.sh

## 3) Uwagi

* Jesli chcesz zeby skrypt docker_images.sh sciagnał obrazy dockerowe należy dodać obrazy do pliku images_docker.json.

Szablon dodawania obrazow do pliku images_docker.json

```json

{
        "name" : "name_image",
        "tag" : "name_tag"
},

```
