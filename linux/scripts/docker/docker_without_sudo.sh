#!/bin/bash
readonly path_dependency='../../main_dependencies' 
if [ -f "${path_dependency}/function_help.sh" ]; then
	# shellcheck source=/dev/null
	source "${path_dependency}/function_help.sh"
else
	echo -e "Nie ma wymaganego pliku function_help.sh"
	exit 1;
fi


run_docker_without_sudo(){
    read -rp "${CYAN}Czy chcesz uzywac docker bez wpisywania sudo${WHITE}[y/n] " docker_choose
    if [ "$docker_choose" == 'y' ]; then
        groups "${USER}" | grep "docker" &> '/dev/null'
        if [  "$?" -eq 1 ]; then
           sudo usermod -aG docker "${USER}"
           echo -e "${GREEN} Dodano użytkownika ${USER} do grupy docker ${WHITE}"
           newgrp docker
        else
            echo -e "${BLUE} Użytkownik ${USER} jest juz dodany do grupy docker ${WHITE}"
        fi
    fi
}

install_rootless(){
    if docker -v; then
        sudo systemctl disable --now docker.service docker.socket
        sudo apt install -y uidmap curl && curl -fsSL https://get.docker.com/rootless | sh
        export DOCKER_HOST=unix:///run/user/1000/docker.sock
    fi
}

if [ "$1" = '--rootless' ]; then
    install_rootless
else
    echo "${RED}Czy na pewno chcesz wykonać ten skrypt ponieważ wykonanie tego skryptu może zwiekszyć powierzchnie ataku${WHITE}
    https://security.stackexchange.com/questions/178542/is-adding-docker-group-not-a-good-idea
    "
    read -rp "[y/n] " choose 
    if [ "$choose" = 'y' ];then
        run_docker_without_sudo
    else
        echo "${YELLOW}Uruchom skrypt z paramerem --rootless aby ustawić rootless mode w docker -> https://docs.docker.com/engine/security/rootless/ ${WHITE}"
    fi
fi
