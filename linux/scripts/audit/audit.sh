#!/bin/bash
set -uo pipefail

audit(){
    data="$(date +"%H:%M_%d-%m-%Y")"
    lynis --version ||  git clone https://github.com/CISOfy/lynis &&  
    lynis audit system > "raport_${data}_lynis"
    systemd analyze security > "raport_${data}_systemd"
}
audit

