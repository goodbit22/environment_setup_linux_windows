#!/bin/bash
set -u 

readonly path_dependency='../../main_dependencies' 
# shellcheck source=/dev/null
if [ -f "${path_dependency}/function_help.sh" ]; then
	source "${path_dependency}/function_help.sh"
else
	echo -e "Nie ma wymaganego pliku function_help.sh"
	exit 1;
fi

zsh_configuration(){
    local zsh_plugins=("zsh-autosuggestions" "zsh-syntax-highlighting" "docker" "docker-compose" "colorize" "command-not-found" "vscode"
    "web-search" "colored-man-pages" "gitfast" "kubectl" "helm" "terraform" "pip" "virtualenv" "argocd" "pip")
    local -rA zsh_plugins_github=(['zsh-autosuggestions']='https://github.com/zsh-users/zsh-autosuggestions.git'
    ['zsh-syntax-highlighting']='https://github.com/zsh-users/zsh-syntax-highlighting.git' )
    sed -i "s/^plugins.*)$/plugins=(${zsh_plugins[*]})/g" ~/.zshrc
    less ~/.zshrc |  grep -E  "^ZSH_THEME=.robbyrussell.$" 
    sed -i "s/robbyrussell/powerlevel10k\/powerlevel10k/g" ~/.zshrc
    less ~/.zshrc |  grep -E  "^ZSH_THEME=*"
    for plugin in "${!zsh_plugins_github[@]}"; do
        git clone "${zsh_plugins_github[$plugin]}" "${HOME}/.oh-my-zsh/custom/plugins/${plugin}"
        echo "${GREEN}Pobrano plugin: $plugin ${WHITE}"
	done
    git clone https://github.com/romkatv/powerlevel10k ~/.oh-my-zsh/custom/themes/powerlevel10k
    echo "${GREEN} Pobrano themes ${WHITE}"
    wget https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/Hack/Regular/complete/Hack%20Regular%20Nerd%20Font%20Complete.ttf
    sudo mv 'Hack Regular Nerd Font Complete.ttf' '/usr/share/fonts'
    mkdir -p ~/.local/share/fonts
    cd ~/.local/share/fonts && curl -fLo "Droid Sans Mono for Powerline Nerd Font Complete.otf" https://github.com/ryanoasis/nerd-fonts/raw/HEAD/patched-fonts/DroidSansMono/complete/Droid%20Sans%20Mono%20Nerd%20Font%20Complete.otf
    #wget 'https://github.com/ryanoasis/nerd-fonts/raw/master/patched-fonts/FiraCode/Medium/complete/Fira%20Code%20Medium%20Nerd%20Font%20Complete.ttf'
    echo "${GREEN} Pobrano czcionki ${WHITE}"
    echo "Nalezy uzyc polecenia: ${CYAN}source ~/.zshrc${WHITE} w celu przeladowania powloki"
    echo  "source <(kubectl completion zsh)" >> ~/.zshrc 
}

if [ "$SHELL" = '/usr/bin/zsh' ]; then
    zsh_configuration
fi 