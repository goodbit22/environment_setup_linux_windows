# 1)Opis skryptów

    zsh_configuration.sh - skrypt do konfigurowania powłoki zsh (uzyc po zainstalowaniu zsh i oh_my_zsh)
    bash_configuration.sh - skrypt do konfigurowania powłoki bash
    tmux_configuration.sh - skrypt do konfigurowania tmux

## 2)Uruchamianie skryptów

1. komendy:

* sudo bash zsh_configuration.sh
* sudo bash bash_configuration.sh
* sudo bash tmux_configuration.sh
