#!/bin/bash

plugins_tmux(){
    [ ! -d "${HOME}/.tmux/plugins/tpm" ] && git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
    if [ ! -f "${HOME}/.tmux.conf" ]; then

        cat <<- EOF > "${HOME}/.tmux.conf"
            unbind r
            bind r source-file ~/.tmux.conf
            set-option -g history-limit 25000

            #option
            set -g mouse on

            # Start windows and panes at 1, not 0
            set -g base-index 1
            set -g pane-base-index 1
            set-window-option -g pane-base-index 1
            set-option -g renumber-windows on

            # List of plugins 
            set -g @plugin 'tmux-plugins/tpm'
            set -g @plugin 'tmux-plugins/tmux-sensible'
            set -g @plugin 'tmux-plugins/tmux-resurrect'
            set -g @plugin 'tmux-plugins/tmux-continuum'
            set -g @plugin 'christoomey/vim-tmux-navigator'
            set -g @plugin 'dracula/tmux'
            set -s default-terminal 'tmux-256color'

            # dracula customizations
            set -g @dracula-plugins "battery cpu-usage ram-usage time"
            set -g @dracula-show-powerline false
            set -g @dracula-show-fahrenheit false
            set -g @dracula-military-time true
            set -g @dracula-show-location false

            # Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
            run '~/.tmux/plugins/tpm/tpm'
EOF
        tmux source ~/.tmux.conf
        echo "Nacisnij ctrl-B + I gdy odpalis tmux"
    fi
}

if tmux -V; then
    plugins_tmux
fi 