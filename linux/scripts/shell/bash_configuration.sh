#!/bin/bash
set -u 
readonly path_dependency='../../main_dependencies' 
# shellcheck source=/dev/null
if [ -f "${path_dependency}/function_help.sh" ]; then
	source "${path_dependency}/function_help.sh"
else
	echo -e "Nie ma wymaganego pliku function_help.sh"
	exit 1;
fi

bash_configuration(){
	local plugins=('node' 'nvm' 'man' 'java' 'ssh' 'python' 'git' 'docker' 'docker-machine'  'gradle' 'docker-compose')
	git clone --depth=1 https://github.com/Bash-it/bash-it.git ~/.bash_it &&  ~/.bash_it/install.sh
	echo -e "bash-it enable alias all \nbash-it enable completion all" >> ~/.bashrc
	echo "bash-it enable plugin ${plugins[*]}" >> ~/.bashrc
	sed -i "s/bobby/brainy/g" ~/.bashrc
}

if [ "$SHELL" = '/bin/bash' ]; then
    bash_configuration
fi 

