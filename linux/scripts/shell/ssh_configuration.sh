#!/bin/bash
create_config(){
    if [[ ! -f "${HOME}/.ssh/config"  ]];then
        [ ! -f "${HOME}/.ssh" ] && mkdir -p "${HOME}/.ssh"
cat <<- _EOF_ > "${HOME}/.ssh/config"
Host github.com
  User git
  HostName github.com 
  IdentityFile ~/.ssh/github
  TCPKeepAlive yes
  IdentitiesOnly yes
HostName gitlab.com 
    User git
    IdentityFile ~/.ssh/gitlab
    TCPKeepAlive yes
    IdentitiesOnly yes
    AddKeysToAgent yes
_EOF_
    fi    
}

create_config