#!/bin/bash

install_pycharm(){
    if [ ! -f '/opt/pycharm-community/bin/pycharm.sh' ]; then
        curl -L "https://download.jetbrains.com/python/pycharm-community-${LATEST_VERSION_PYCHARM}.tar.gz" -o pycharm-community.tar.gz && \
        sudo tar -xzf pycharm-community.tar.gz -C /opt && \
        rm -r pycharm-community.tar.gz && \
        sudo chmod u+x "/opt/pycharm-community-${LATEST_VERSION_PYCHARM}/bin/pycharm.sh" && \
        sudo mv "/opt/pycharm-community-${LATEST_VERSION_PYCHARM}" "/opt/pycharm-community" && \
        sudo cp '/main_dependencies/files/application/pycharm-community.desktop' /usr/share/applications && \
        echo "Wgrano pycharm"
        #sudo bash "/opt/pycharm-community/bin/pycharm.sh" &
    fi
}


add_pannel(){
    elements=('ghidra.desktop' 'ida.dekstop' 'ideaIC.desktop' 'pycharm-community.desktop') 
    number=10
    if $DESKTOP_SESSION  -eq 'xcfe'; then 
        for element in "${elements[@]}"
        do
            mkdir -p "$HOME/.config/xfce4/panel/launcher-$number/"
            cp "/usr/share/applications/$element" "$HOME/.config/xfce4/panel/launcher-$number/"
            cat  "X-XFCE-Source=file:///usr/share/applications/${element}" >> "$HOME/.config/xfce4/panel/launcher-$number/${element}"
            number=$((number + 1))
        done 
    fi 
}

add_profile_d(){
    cat <<- _EOF_ >> '/etc/profile.d/ghidra.sh'
  export GHIDRA_HOME=/opt/ghidra/latest
  export PATH=${GHIDRA_HOME}:${PATH}
_EOF_
    cat <<- _EOF_ >> '/etc/profile.d/gradle.sh'
  export GRADLE_HOME=/opt/gradle/latest
  export PATH=${GRADLE_HOME}/bin:${PATH}
_EOF_
echo "source /etc/profile.d/ghidra.sh" >> ~/.zshrc
echo "source /etc/profile.d/gradle.sh" >> ~/.zshrc
}

install_ghidra(){
    if (ghidra &> '/dev/null'); then
        local ghidra_directory='/opt/ghidra'
        local ghidra_zip="${GHIDRA_LINK_DOWLOAND##*/}"
        echo "Instalacja GHIDRA"
        wget "${GHIDRA_LINK_DOWLOAND}" -P /tmp
        sudo mkdir -p "${ghidra_directory}"
        sudo unzip "$ghidra_zip" -d "${ghidra_directory}"
        sudo ln -s "/opt/ghidra/${GHIDRA_VERSION}" "${ghidra_directory}/latest"
        sudo mv "/opt/ghidra/ghidra_${GHIDRA_VERSION}/ghidraRun" "/opt/ghidra/ghidra_${GHIDRA_VERSION}/ghidra"
        sudo cp '/main_dependencies/files/application/ghidra.desktop' /usr/share/applications
    fi
}

install_gradle(){
    if (gradle -v &> '/dev/null'); then
        local gradle_directory='/opt/gradle'
        sudo mkdir "$gradle_directory"
        wget "https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip" -P /tmp
        sudo unzip -d "$gradle_directory" "/tmp/gradle-${GRADLE_VERSION}-bin.zip"
        sudo ln -s "/opt/gradle/gradle-${GRADLE_VERSION}" '/opt/gradle/latest'
    fi
}

install_ida(){
    declare -r LINK_IDA_DOWNLOAD="https://out7.hex-rays.com/files/idafree${IDA_FREEWARE_VERSION}_linux.run"
    declare -r IDA_FILENAME="${LINK_IDA_DOWNLOAD##*/}"
    declare -r IDA_DOWLOAND_PATH="/tmp/${IDA_FILENAME}"
    echo "Instalacja IDA FREEWARE"
    terminator-x "curl -o ${IDA_DOWLOAND_PATH} -fSL $LINK_IDA_DOWNLOAD && chmod +x ${IDA_DOWLOAND_PATH} && ${IDA_DOWLOAND_PATH}"
    sudo cp '/main_dependencies/files/application/ida.desktop' /usr/share/applications
}


install_intellij_community(){
   curl -L "https://download.jetbrains.com/idea/ideaIC-${LATEST_VERSION_Intellij}.tar.gz" -o ideaIC.tar.gz && \
   mkdir -p /opt/ideaIC && \
   sudo tar -xvzf ideaIC.tar.gz --strip-components=1 -C /opt/ideaIC && \
   sudo chmod u+x /opt/ideaIC/bin/idea.sh && \
   sudo cp '/main_dependencies/files/application/ideaIC.desktop' /usr/share/applications &&  \
   rm -r ideaIC.tar.gz && \
   echo "Wgrano intellij"
   #sudo bash /opt/ideaIC/bin/idea.sh &
}

install_packages_nixos(){
    sh <(curl -L https://nixos.org/nix/install) --daemons
}


install_helm(){
    if  ! dpkg-query -l 'helm'; then
        curl -fsSL -o get_helm.sh 'https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3' && chmod 700 get_helm.sh
        terminator -x ./get_helm.sh
    fi
}

install_kubectl(){
    if [ ! -f  '/usr/bin/kubectl' ]; then
        curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" && \
        curl -LO "https://dl.k8s.io/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl.sha256" && \
        echo "$(cat kubectl.sha256)  kubectl" | sha256sum --check ; rm kubectl.sha256  && \
        sudo  install  kubectl /usr/bin/kubectl
    fi
}

install_packages_rpm(){
    readonly name_directory="rpm_packages"
    local packages_rpm=("https://github.com/PowerShell/PowerShell/releases/download/v${POWERSHELL_VERSION}/powershell-${POWERSHELL_VERSION}.rh.x86_64.rpm"
    "https://github.com/wagoodman/dive/releases/download/v${DIVE_VERSION}/dive_${DIVE_VERSION}_linux_amd64.rpm"
    "https://dbeaver.io/files/dbeaver-ce-latest-stable.x86_64.rpm"
    "https://github.com/fujiapple852/trippy/releases/download/${TRIPPY_VERSION}/trippy-${TRIPPY_VERSION}-x86_64.rpm")
    [ ! -d  "$name_directory" ] && mkdir "$name_directory" && \
    echo -e "${GREEN}Katalog $name_directory zostal utworzony${WHITE}"
    for package_rpm in "${packages_rpm[@]}" 
    do
       curl -L "$package_rpm" -o  "${name_directory}/${package_rpm##*/}}" &&  sudo dnf install "${package_rpm##*/}"
    done
    [ -d  "$name_directory" ] &&  rm -rf "$name_directory" && \
    echo -e "${GREEN}Katalog $name_directory zostal usuniety wraz z zawartoscia${WHITE}"
}

install_packages_deb(){
    readonly name_directory="deb_packages"
    local packages_deb=("https://packages.microsoft.com/config/ubuntu/$(lsb_release -rs)/packages-microsoft-prod.deb"
    "https://github.com/wagoodman/dive/releases/download/v${DIVE_VERSION}/dive_${DIVE_VERSION}_linux_amd64.deb"
    "https://github.com/fujiapple852/trippy/releases/download/${TRIPPY_VERSION}/trippy_x86_64-unknown-linux-gnu_${TRIPPY_VERSION}_amd64.deb")
    [ ! -d  "$name_directory" ] && mkdir "$name_directory" &&  echo -e "${GREEN}Katalog $name_directory zostal utworzony${WHITE}"
    for package_deb in "${packages_deb[@]}" 
    do
        curl -L "$package_deb" -o "${name_directory}/${package_deb##*/}" && sudo dpkg -i "${name_directory}/${package_deb##*/}"
    done
    [ -d  "$name_directory" ] && rm -rf "$name_directory" && \
    echo -e "${GREEN}Katalog $name_directory zostal usuniety wraz z zawartoscia${WHITE}"
    sudo apt install -y powershell
}

minikube_configuration(){
    if [ ! -f ' /usr/local/bin/minikube' ];then 
        sudo curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 && \
        sudo install minikube /usr/local/bin/minikube && \
        sudo rm minikube && \
        minikube start && \
        minikube addons enable ingress && \
        minikube addons enable dashboard && \
        minikube addons enable metrics-server && \
        printf '\n\n\033[4;33m Enabled Addons \033[0m' && \
        minikube addons list | grep STATUS && minikube addons list | grep enabled && \
        printf '\n\n\033[4;33m Current status of Minikube \033[0m' && minikube status
    fi
}

install_software_github(){
    declare -rA github_softwares=(["lazydocker"]="https://raw.githubusercontent.com/jesseduffield/lazydocker/master/scripts/install_update_linux.sh" 
    ["k9s"]="https://webinstall.dev/k9s" ["kustomize"]="https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh"
    ["act"]="https://raw.githubusercontent.com/nektos/act/master/install.sh" ["tfsec"]="https://raw.githubusercontent.com/aquasecurity/tfsec/master/scripts/install_linux.sh"
    )
    for software in "${!github_softwares[@]}" ;do 
        if [ ! -f "/usr/bin/$software" ];then
            curl -s "${github_softwares[$software]}" | bash;
        fi
    done
    declare -rA github_softwares_curl=(['container-structure-test']="https://storage.googleapis.com/container-structure-test/latest/container-structure-test-linux-amd64"
    ['hadolint']="https://github.com/hadolint/hadolint/releases/download/v${HADOLINT_VERSION}/hadolint-Linux-x86_64" ['skaffold']=' https://storage.googleapis.com/skaffold/releases/latest/skaffold-linux-amd64 ')
    for software in "${!github_softwares_curl[@]}" ;do 
        if [ ! -f "/usr/bin/$software" ];then
            curl -L "${github_softwares_curl[$software]}" -o "${github_softwares_curl[$software]##*/}" && \
            sudo install "${github_softwares_curl[$software]##*/}"  "/usr/bin/$software" && \
            rm  "${github_softwares_curl[$software]##*/}"
        fi
    done
    [ ! -f '/usr/bin/lazydocker' ] &&  sudo cp "$HOME/.local/bin/lazydocker" '/usr/bin'
    [ ! -f  '/usr/bin/kustomize' ] && sudo  mv 'kustomize' '/usr/bin'
    [ ! -f  '/usr/bin/k9s' ] &&  sudo cp "/$USER/.local/bin/k9s" '/usr/bin'
    [ ! -f  "$HOME/bin/act" ] && mv bin "$HOME"
}

ansible_package_install(){
    declare -r ansible_packages=('community.docker')
    for ansible_package in "${ansible_packages[@]}";do
        ansible-galaxy collection install  "${ansible_package}"
    done
}

install_go_packages(){
    local go_packages=('github.com/go-delve/delve/cmd/dlv@latest')
    for package in "${go_packages[@]}"
    do
        go install "$package"
    done
}

install_rust_packages(){
    local rust_packages=('tplay')
    for package in "${rust_packages[@]}";do 
        rust "$package" 
    done

}

configure_tools(){
    local path_script_main="$PWD" 
    echo -e "\nTrwa Konfiguracja vima \n"
	git clone https://gitlab.com/goodbit22/instviplug.git && ( cd instviplug || return 1 )
	sudo terminator -x bash  main.sh 
    cd "$path_script_main" || return 1
    echo -e "${CYAN}Aby dokonczyc setup zsh nalezy uruchomić skrypt  [ zsh_configuration.sh ]${WHITE}"
    sudo terminator -x  'sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"'
}

kubectl_plugins(){
  if ! command -v kubectl-krew &> /dev/null; then
    echo "krew is not installed. Installing..."  
    (
    set -x; cd "$(mktemp -d)" &&
    OS="$(uname | tr '[:upper:]' '[:lower:]')" &&
    ARCH="$(uname -m | sed -e 's/x86_64/amd64/' -e 's/\(arm\)\(64\)\?.*/\1\2/' -e 's/aarch64$/arm64/')" &&
    KREW="krew-${OS}_${ARCH}" &&
    curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/${KREW}.tar.gz" &&
    tar zxvf "${KREW}.tar.gz" &&
    ./"${KREW}" install krew
    )
    echo "export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH""  >> "${HOME}/.zshrc"
    kubectl krew install ktop 
    kubectl krew version
    echo "krew installed successfully."
    else
        echo "krew is already installed."
    fi
}

common(){   
    minikube_configuration
    install_software_github
    docker_test
    docker-compose_update  
    configure_tools
    kubectl_plugins
}

main_install(){
    DIVE_VERSION=$(curl -s "https://api.github.com/repos/wagoodman/dive/releases/latest" | grep -Po '"tag_name": "v\K[0-9.]+')
    HADOLINT_VERSION=$(curl -s "https://api.github.com/repos/hadolint/hadolint/releases/latest"  | grep -Po '"tag_name": "v\K[0-9.]+')
    LATEST_VERSION_PYCHARM=$(curl -s 'https://data.services.jetbrains.com/products/releases?code=PCP' | grep -Po '"version":"\K[0-9]*(\.[0-9]*)*(b[0-9]*)?' | head -1)
    LATEST_VERSION_Intellij=$(curl -s 'https://data.services.jetbrains.com/products/releases?code=IIC' | grep -Po '"version":"\K[0-9]*(\.[0-9]*)*(b[0-9]*)?' | head -1)
    POWERSHELL_VERSION=$(curl -s "https://api.github.com/repos/PowerShell/PowerShell/releases/latest" | grep -Po '"tag_name": "v\K[0-9.]+')
    GRADLE_VERSION=$(curl -s "https://api.github.com/repos/gradle/gradle/releases/latest" | grep -Po '"tag_name": "v\K[0-9.]+')
    GHIDRA_VERSION=$(curl -s "https://api.github.com/repos/NationalSecurityAgency/ghidra/releases/latest" | jq -r '.name' | awk '{print $2}')
    GHIDRA_LINK_DOWLOAND=$(curl -s "https://api.github.com/repos/NationalSecurityAgency/ghidra/releases/latest" | jq -r '.assets[].browser_download_url')
    IDA_FREEWARE_VERSION=$(curl -s https://hex-rays.com/products/ida/support/%20download_freeware%20/ | grep -m 1 -Eo  'idafree[0-9]{2,}_linux.run')
    TRIPPY_VERSION=$(curl  -s "https://api.github.com/repos/fujiapple852/trippy/releases/latest" |  jq -r '.tag_name')
    check_distribute="$(cat < '/etc/os-release' | grep -w NAME | sed 's/NAME=//g' | tr -d '"')" 
    check_distribute_linux
	if [ "$check_distribute" == "Fedora" ]; then
        install_software_fedora
        install_kubectl
        install_pycharm
        install_packages_rpm 
	elif [ "$check_distribute" == "openSUSE Leap" ];then
		install_software_suse 
        install_helm
        install_kubectl
        install_pycharm
        install_packages_rpm
	else
        install_software_ubuntu
        install_packages_deb
        install_pycharm
        install_kubectl
	fi
    common
}