#!/bin/bash 

add_repository_suse(){
    sudo zypper --gpg-auto-import-keys refresh
    sudo zypper refresh
}

nvidia_install(){
    if (lspci | grep -E "VGA.*NVIDIA") ; then
        sudo zypper install openSUSE-repos-NVIDIA
        sudo zypper install-new-recommends --repo NVIDIA
    fi
}

install_software_suse(){
    source '/etc/os-release'
    opensuse_version="$(echo $NAME | awk '{print $2}' | tr '[:upper:]' '[:lower:]')"
    add_repository_suze
    local packages_suse=("flatpak" "vim" "shellcheck" "virtualbox" "kazam" "keepass2" "terminator" "docker" "docker-compose" "virtualbox-guest-additions-iso"
    "vagrant"  "virtualbox-ext-pack" "curl" "ansible" "make" "auditd" 'htop' 'git' 'clamav' "wget" "tmux"
    "packer" "bandit" "bpython"  "fzf" "code" 'containerd.io' 'docker-compose-plugin' 'docker-ce docker-ce-cli' 'lynis' 'python3-docker-compose'
    'patterns-openSUSE-kvm_server' 'patterns-server-kvm_tools' 'bridge-utils' 'zsh' 'chkrootkit' 'gdb' 'sysstat' 'openjdk17-jdk')
    echo "Czy chcesz zainstalowac "
    echo "pakiety dostepne w suse: ${CYAN}${packages_suse[*]}${WHITE}"
    local choose_install=0
    until [[ "$choose_install" -ge 1 && "$choose_install" -le 3 ]]; do
        cat <<- _EOF_
        1.Zainstaluj wszystko pakiety
        2.Zainstaluj wybrane pakiety
        3.Pomin instalacje wszystkich pakietow
_EOF_
        read -rp "Wybierz opcje: " choose_install
    done
    for package_suse in "${packages_suse[@]}"; do
        if [ "$choose_install" -eq 1 ];then 
            echo -e "${BLUE}Trwa instalowanie $package_suse ${WHITE}"
            sudo zypper install -y "${packages_suse[@]}" 
            break
        elif [ "$choose_install" -eq 2 ]; then
            read -rp "${CYAN}Czy chcesz zainstalowac $package_suse ${WHITE}[y/n] " choose_install_soft
            if [ "$choose_install_soft" = "y" ]; then
                echo  -e "${YELLOW}Trwa instalowanie $package_suse ${WHITE}"
                sudo zypper install -y "$package_suse"
            else
                echo "${YELLOW} Pominieto instalowanie $package_suse ${WHITE}"
            fi
        elif [ "$choose_install" -eq 3 ]; then
            echo -e "${YELLOW}Pominieto instalacje ${packages_suse[*]} ${WHITE}"
            break
        fi
    done
    sudo systemctl enable docker && sudo systemctl start docker
    sudo systemctl enable libvirtd && sudo systemctl restart libvirtd && ( sudo modprobe kvm-intel || sudo modprobe kvm-amd )
}