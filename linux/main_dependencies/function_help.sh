#!/bin/bash
set -u

authorization(){
	if [ "$UID" -eq 0 ];then
		clear
	else
		echo -e "${RED}Permission denied run the script with sudo ${WHITE}"; exit 1
	fi
}

playbook_run(){
	plabyooks=('main_compose_secret.yml' 'add_dnsname.yml' 'add_repositories.yml' 'install_packages.yml')
	for plabyook in $plabyooks; do
		ansible-playbook  "/playbooks/${plabyook}" localhost
	done
}

check_network_connection(){
	status=("${GREEN}[OK]" "${YELLOW}[WARNING]" "${RED}ERROR" )
	ping -c 2 www.google.pl >> /dev/null
	echo -e "${CYAN}Dostep do sieci${WHITE} ${status["$?"]}"
}

check_distribute_linux(){
	echo -e "Sprawdzanie dystrybucji"
	local linux_distribute=("Debian" "Ubuntu" "Kali" "Fedora" "openSUSE" "Peppermint" "Mint")
	check_distribute="$(cat < '/etc/os-release' | grep -w NAME | sed 's/NAME=//g' | tr -d '"' | awk '{print $1}')" 
	local count=1
	for distribute  in "${linux_distribute[@]}"
	do
		if [ "$check_distribute" == "$distribute" ];then
			clear
			break
		elif [ "${#linux_distribute[*]}" -eq "$count" ]; then 
			echo -e "${RED}Twojej dystrybucji nie ma na liscie${WHITE}"
			exit 1
		fi
		((count++))
	done
}

docker-compose_test(){
    echo -e "${CYAN}Trwa sprawdzanie czy docker-compose dobrze działa${WHITE}"
    sudo docker-compose -f  ./files/docker_compose_Test.yml up <& '/dev/null' && sudo docker-compose -f ./files/docker_compose_Test.yml rm -f  <& '/dev/null'
    local -r status_code="$?"
	[ "$status_code" -eq 0 ] && echo -e "${GREEN}docker-compose dobrze działa${WHITE}" || echo -e "${RED}Wystapil problem w działaniu docker-compose${WHITE}"
}

docker_test(){
    echo -e "${YELLOW}Trwa sprawdzanie czy docker dobrze działa ${WHITE}"
    sudo docker run --rm hello-world  <& '/dev/null' && sudo docker rmi hello-world  <& '/dev/null'
	local -r status_code="$?"
	[ "$status_code" -eq 0 ] && echo -e "${GREEN}docker dobrze działa ${WHITE}" || echo -e "${RED}Wystapil problem w działaniu docker${WHITE}"
}

docker-compose_update(){
    local latest_version=$(curl -s  https://api.github.com/repos/docker/compose/releases/latest  | grep -Po '"tag_name": "v\K[0-9.]+') || echo '2.7.0'
    docker_compose_version_latest=$(awk -v ver_current="$(docker-compose -v | awk '{print $3}' | tr -d '.')" -v ver_last="$(echo ${latest_version} | tr -d '.')" 'BEGIN { print (ver_current >= ver_last ) ? "YES" : "NO" }')
    if [ "$docker_compose_version_latest" == "NO" ];then                                                      
        echo "Trwa Aktualizacja docker-compose do najnowszej wersji"                                            
		sudo apt remove -y docker-compose  || sudo yum remove -y docker-compose  || sudo zypper remove -y docker-compose
		[ ! -d "/usr/local/lib/docker/cli-plugins" ] && sudo mkdir -p '/usr/local/lib/docker/cli-plugins'
 		sudo curl -SL https://github.com/docker/compose/releases/download/v${latest_version}/docker-compose-linux-x86_64 -o /usr/local/lib/docker/cli-plugins/docker-compose
		sudo chmod +x /usr/local/lib/docker/cli-plugins/docker-compose
		sudo ln -s /usr/local/lib/docker/cli-plugins/docker-compose /usr/bin/docker-compose
        docker-compose_test
		docker-compose --version && echo -e "${GREEN}Zaktualizowano wersje docker-compose do najnowszej${WHITE}" || \
		echo -e "${RED}Nie udalo sie zaktualizowac wersji docker-compose do najnowszej${WHITE}"                                    
    else
        echo -e "${GREEN}docker-compose jest aktualny${WHITE}"
    fi        
}

docker_plugins(){
	curl -fsSL https://raw.githubusercontent.com/docker/scout-cli/main/install.sh -o install-scout.sh 
	./install-scout.sh
}

wave_install(){
	WAVE_VERSION=$(curl -s  https://api.github.com/repos/wavetermdev/waveterm/releases/latest  | grep -Po '"tag_name": "v\K[0-9.]+' )
	curl -SL  "https://dl.waveterm.dev/releases/Wave-linux-amd64-${WAVE_VERSION}.deb"  -o "wave-linux-amd64-${WAVE_VERSION}.deb"
	sudo apt install "wave-linux-amd64-${WAVE_VERSION}.deb"
}

RED=$(tput setaf 1) 
GREEN=$(tput setaf 2) 
YELLOW=$(tput setaf 3)
BLUE=$(tput setaf 4)
MAGENTA=$(tput setaf 5)
CYAN=$(tput setaf 6)
WHITE=$(tput setaf 7)
COLUMNS=$(tput cols)