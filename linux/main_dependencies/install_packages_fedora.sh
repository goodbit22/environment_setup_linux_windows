#!/bin/bash

add_repository_fedora(){
    if [ ! -f '/etc/yum.repos.d/vscode.repo' ];then
        sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
cat <<-EOF | sudo tee /etc/yum.repos.d/vscode.repo
[code]
name=Visual Studio Code
baseurl=https://packages.microsoft.com/yumrepos/vscode
enabled=1
gpgcheck=1
gpgkey=https://packages.microsoft.com/keys/microsoft.asc
EOF
    fi

    if [ -f "/etc/yum.repos.d/kubernetes.repo" ];then
     cat <<- EOF >  "/etc/yum.repos.d/kubernetes.repo"
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-\$basearch
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
    fi 
    sudo dnf upgrade --refresh
}

nvidia_install(){
    if (lspci | grep -E "VGA.*NVIDIA") ; then
        sudo dnf install kernel-headers kernel-devel tar bzip2 make automake gcc gcc-c++ pciutils elfutils-libelf-devel libglvnd-opengl libglvnd-glx libglvnd-devel acpid pkgconfig dkms
        sudo dnf module install nvidia-driver:latest-dkms
    fi 
}


install_software_fedora(){
    source '/etc/os-release'
    version_fedora="$VERSION"
    add_repository_fedora
    local packages_dnf=("jq"  "curl" "gpg" 'flatpak' 'git' 'terminator' 'htop' 'vim' 'clamav'
    'clamav-update' 'docker-ce docker-ce-cli' 'containerd.io' 'docker-compose' "shellcheck" "wget" "tmux" "packer" "auditd"
    "virtualbox" "kazam" "keepass2" "ansible" "vagrant"  "make"  "virtualbox-guest-additions-iso" "virtualbox-ext-pack" 
    "terraform" 'bridge-utils libvirt' 'virt-install' 'qemu-kvm' 'libvirt-devel' 'virt-top' 'libguestfs-tools' 'guestfs-tools' 'virt-manager' 'fzf'
    "lxqt" "zsh" "bpython" "bandit"  "code" 'lynis' 'helm' 'powershell' 'java-17-openjdk-devel' 'chkrootkit' 'gdb' 'sysstat') 
    echo "Czy chcesz zainstalowac "
    echo "pakiety dostepne w dnf: ${CYAN}${packages_dnf[*]}${WHITE}"
    choose_install=0
    until [[ "$choose_install" -ge 1 && "$choose_install" -le 3 ]]; do
        cat <<- _EOF_
        1.Zainstaluj wszystko pakiety
        2.Zainstaluj wybrane pakiety
        3.Pomin instalacje wszystkich pakietow
_EOF_
        read -i "3" -rpe "Wybierz opcje: " choose_install
    done
    for package_dnf in "${packages_dnf[@]}"; do
        if [ "$choose_install" -eq 1 ];then 
            echo -e "${BLUE}Trwa instalowanie $package_dnf ${WHITE}"
            sudo dnf install -y "${packages_dnf[@]}" 
            break
        elif [ "$choose_install" -eq 2 ]; then
            read -rp "${CYAN}Czy chcesz zainstalowac $package_dnf${WHITE}[y/n] " choose_install_soft
            if [ "$choose_install_soft" = "y" ]; then
                echo  -e "${YELLOW}Trwa instalowanie $package_dnf ${WHITE}"
                sudo dnf install -y "$" 
            else
                echo "${YELLOW} Pominieto instalowanie $package_dnf ${WHITE}"
            fi
        elif [ "$choose_install" -eq 3 ]; then
            echo -e "${YELLOW}Pominieto instalowanie ${packages_dnf[*]} ${WHITE}"
            break
        fi
    done
    sudo systemctl start docker && sudo systemctl enable docker 
    sudo systemctl start libvirtd && sudo systemctl enable libvirtd
}