# Opis zależność wykorzystywanych w głównym skrypcie

    function_help.sh - zawiera funkcje wykorzystywane w wielu skryptach
    install_software.sh - zawiera funkcje do instalacji  pakietów dla wszystkich linuxów
    install_packages_suze.sh - zawiera funkcje do instalacji pakietów dla dystrybucji opartych na Suze
    install_packages_fedora.sh - zawiera funkcje do instalacji pakietów dla dystrybucji opartych na Red hat
    install_packages_debian.sh - zawiera funkcje do instalacji pakietów dla dystrybucji opartych na Debian
