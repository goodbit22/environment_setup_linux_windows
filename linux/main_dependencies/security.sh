#!/usr/bin/bash

disable_kernel_modules(){
	modprobe --showconfig | grep blacklist
	modules=( 'thunderbolt' 'firewire-core' 'floppy' 'nouveau' 'pcspkr')
	for module in "${modules[@]}";
	do
		if  ! modprobe --showconfig | grep -w blacklist "$module" &> '/dev/null'; then
			echo "Disable $module kernel module"
			echo "blacklist $module" >> '/etc/modprobe.d/blacklist-modules.conf'
		fi 
	done
}

enable_App_Armor(){
	[[ $( cat /sys/module/apparmor/parameters/enabled) != "Y" ]] &&	sudo apt -y install apparmor 
}

hardening_kernel_parameters(){
	parameters=(
		'kernel.exec-shield=1'
		'kernel.randomize_va_space=1' # Turn on execshield
		'net.ipv4.conf.all.rp_filter=1' 	# Enable IP spoofing protection
		'net.ipv4.conf.all.accept_source_route=0' 		# Disable IP source routing
		'net.ipv4.icmp_echo_ignore_broadcasts=1'
		'net.ipv4.icmp_ignore_bogus_error_messages=1'		# Ignoring broadcasts request
		'net.ipv4.conf.all.log_martians = 1' 		# Make sure spoofed packets get logged
		'net.ipv6.conf.all.disableipv6 = 1' #Permanently disable IPv6 
		'net.ipv6.conf.default.disableipv6 = 1'
	)
	if [ -f '/etc/sysctl.conf' ]; then
		for parameter in "${parameters[@]}";
		do
			if grep -q "$parameter" /etc/sysctl.conf; then
				sed -i "s/^.*$parameter.*$/$parameter/g" /etc/sysctl.conf
			else
				echo "$parameter" >> /etc/sysctl.conf
			fi
		done
		sysctl -p
		echo "Reload sysctl.conf file"
	fi
}

install_usbguard(){
  	sudo apt update && \
	sudo apt install -y --no-install-recommends -V \
    	asciidoc autoconf automake bash-completion build-essential catch2 \
    	docbook-xml docbook-xsl git ldap-utils libaudit-dev libcap-ng-dev \
    	libdbus-glib-1-dev libldap-dev libpolkit-gobject-1-dev libprotobuf-dev \
    	libqb-dev libseccomp-dev libsodium-dev libtool libxml2-utils \
    	libumockdev-dev pkg-config protobuf-compiler sudo tao-pegtl-dev xsltproc
	git clone https://github.com/USBGuard/usbguard.git
	cd 'usbguard' || exit 1
	./configure && make && make check && sudo make install
	sudo systemctl enable usbguard && sudo systemctl start usbguard && cd ..
	echo "USBGuard has been installed successfully!" && rm -r usbguard
}

microcode() {
	company=$(cat < /proc/cpuinfo | grep 'model name' | awk '{print $4}' | head -n 1 )
	[ "$company" = 'AMD' ] && ( sudo apt install -y  amd64-microcode ||  sudo yum install -y linux-firmware )
	[ "$company" != 'AMD' ] && ( sudo apt install -y intel-microcode || sudo yum install -y  microcode_ctl)
}

pam_configure(){
	if [ -f '/etc/pam.d/passwd' ]; then
		echo "password required pam_pwquality.so retry=3 minlen=12 difok=6 dcredit=-1 ucredit=-1 ocredit=-1 lcredit=-1 [badwords=myservice mydomain] enforce_for_root" >> '/etc/pam.d/passwd'
		echo "password required pam_unix.so use_authtok sha512 shadow" >> '/etc/pam.d/passwd'
	fi
}

configure_iptables(){
    iptables -L
}

install_selinux(){
    sudo bash -c 'sudo apt install -y selinux-basics selinux-utils selinux-policy-default auditd audispd-plugins >/dev/null 2>&1 & disown' 
	sudo selinux-activate
	echo "ustawinie Selinux w trybie permisive"
	setenforce permissive
	sudo selinux-config-enforcing
	getenforce
	echo "Wyswietlenie wsztskich typow etykiet wewnatrz polityki"
	setinfo -t 
	echo "Wyswietlenie wszyskich userów selinux"
	setinfo -u
	echo "Wyswietlenie wszyskie role selinux"
	setinfo -r
}

install_maldetect(){
    MALDECT_VERSION=$(curl -s https://www.rfxn.com/appdocs/README.maldetect | head -1 | grep -Po '[0-9.]+')  
	CURRENT="$PWD"
	cd /tmp/ && wget http://www.rfxn.com/downloads/maldetect-current.tar.gz && \
	tar xfz maldetect-current.tar.gz && cd "maldetect-${MALDECT_VERSION}" && \
	sudo ./install.sh && cd "$CURRENT" || return 0
}

cron_add_tasks(){
	echo -e "${GREEN}Dodano zadania do cron${WHITE}"
	cat < "cron/cronjob.txt" && crontab "/cron/cronjob.txt"
}

audit_configure(){
	local docker_directories=('/run/containerd' '/var/lib/docker' '/etc/docker' '/usr/bin/docker-containerd' '/usr/bin/docker-containerd' '/usr/bin/docker-runc' 'etc/docker/daemon.json' '/etc/containerd/config.toml' '/usr/bin/containerd-shim' '/etc/default/docker' '/usr/bin/containerd-shim-runc-v2' '/usr/lib/systemd/system/docker.service' '/usr/bin/runc ') 
	for directory in "${docker_directories[@]}"; do
		sudo auditctl -w "$directory" -k docker
		echo "-w $directory" >> /etc/audit/rules.d/docker
	done 
}

install_docker_branch(){
    git clone https://github.com/docker/docker-bench-security.git
    terminator -x 'cd docker-bench-security && sudo sh docker-bench-security.sh && cd .. && rm -r docker-bench-security'
}

install_firejail(){
	git clone https://github.com/netblue30/firejail.git
	terminator -x 'cd firejail && ./configure && make && sudo make install-strip'
	sudo firecfg 
	mkdir ~/.config/firejail
	firejail --list
}

hardening(){
	install_usbguard
	install_maldetect
	install_selinux
	install_docker_branch
	pam_configure
	audit_configure
	disable_kernel_modules
	audit
}

hardening
