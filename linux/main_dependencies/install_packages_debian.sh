#!/bin/bash

add_repository_ubuntu(){
    if [ ! -f '/etc/apt/sources.list.d/librewolf.sources' ]; then
    distro=$(if echo " una bookworm vanessa focal jammy bullseye vera uma " | grep -q " $(lsb_release -sc) "; then lsb_release -sc; else echo focal; fi)  
    wget -O- https://deb.librewolf.net/keyring.gpg | sudo gpg --dearmor -o /usr/share/keyrings/librewolf.gpg
    sudo tee /etc/apt/sources.list.d/librewolf.sources << EOF > /dev/null
Types: deb
URIs: https://deb.librewolf.net
Suites: $distro
Components: main
Architectures: amd64
Signed-By: /usr/share/keyrings/librewolf.gpg
EOF
    fi
    sudo apt update

}

add_repository_debian(){
    if [ ! -f ' /etc/apt/sources.list.d/virtualbox.list' ]; then
        wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
        wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -
        sudo apt install -y software-properties-common
        echo "deb [arch=$(dpkg --print-architecture)] http://download.virtualbox.org/virtualbox/debian $(lsb_release -cs) contrib" | sudo tee /etc/apt/sources.list.d/virtualbox.list
    fi
    sudo apt update
}

nvidia_install(){
    if (lspci | grep -E "VGA.*NVIDIA") ; then
        sudo dpkg --add-architecture i386
        sudo apt install -y libc6:i386 nvidia-driver-libs:i386 
        driver="$(nvidia-detect | grep "It is recommended to install the" -A 1 | tail -n +2 | tr -s '[:space:]')"   
        sudo apt install "$driver"   
    fi 
}

install_software_ubuntu(){
    local packages=("curl" "gpg" "apt-transport-https" "jq" "ca-certificates" "lsb-release" "vim" "shellcheck" "virtualbox" "kazam" 
    "keepass2" "terminator" "docker" "docker-compose" "vagrant" "virtualbox-guest-additions-iso" "virtualbox-ext-pack" "make" "wget" "tmux"
    "bandit" "auditd" "git" "helm" "terraform"  "nala" "fzf" "qemu-kvm" "libvirt-daemon-system" "libvirt-clients" "bridge-utils" "build-essential" "virt-manager"
    "packer" "lxqt" "ansible" "bpython" "code" "clamav-daemon" "clamav" "htop" "dbeaver-ce" "gnupg2" "software-properties-common" 'lynis' 'strace' 'golang' 'ptrace'
     'chkrootkit' 'librewolf' 'build-essential' 'libapparmor-dev' 'pkg-config' 'gawk' 'gdb' 'sysstat' 'openjdk-17-jdk' 'signal-desktop' 'neovim' 'warp')
    add_repository_ubuntu
    echo "pakiety dostepne w apt: ${CYAN}${packages[*]}${WHITE}"
    choose_install=0
    until [[ "$choose_install" -ge 1 && "$choose_install" -le 3 ]]; do 
        echo "1.Zainstaluj wszystko pakiety"
        echo "2.Zainstaluj wybrane pakiety"
        echo "3.Pomin instalacje wszystkich pakietow"
        read -i "3" -erp "${CYAN}Wybierz opcje:${WHITE} " choose_install
    done
    for opt_soft in "${packages[@]}"; do
        if [ "$choose_install" -eq 1 ];then 
            echo -e "${BLUE}Trwa instalowanie $opt_soft ${WHITE}"
            sudo apt install -y "${packages[@]}"
            break
        elif [ "$choose_install" -eq 2 ]; then
            read -i "n" -erp "${CYAN}Czy chcesz zainstalowac $opt_soft ${WHITE}[y/n] " choose_install_soft
            if [ "$choose_install_soft" = "y" ]; then
                echo  -e "${YELLOW}Trwa instalowanie $opt_soft ${WHITE}"
                sudo apt install -y "$opt_soft"
            else
                echo "${YELLOW} Pominieto instalowanie $opt_soft ${WHITE}"
            fi
        elif [ "$choose_install" -eq 3 ]; then
            echo -e "${YELLOW}Pominieto instalowanie $opt_soft ${WHITE}"
        fi
    done
    sudo systemctl enable --now libvirtd
}