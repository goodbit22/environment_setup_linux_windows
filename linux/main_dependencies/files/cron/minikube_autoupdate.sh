#!/bin/bash
MINIKUBE_LATEST_VERSION=$(curl -s "https://api.github.com/repos/kubernetes/minikube/releases/latest" | grep -Po '"tag_name": "v\K[0-9.]+')
MINIKUBE_CURRENT_VERSION=$(minikube version| grep -Eo '[1-9]\.[0-9]{1,2}\.[0-9]')
if "$MINIKUBE_CURRENT_VERSION" != "$MINIKUBE_LATEST_VERSION"; then 
        sudo rm minikube && \
        sudo curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 && \
        sudo install minikube /usr/local/bin/minikube && \
        minikube start && \
        minikube addons enable ingress && \
        minikube addons enable dashboard && \
        minikube addons enable metrics-server && \
        printf '\n\n\033[4;33m Enabled Addons \033[0m' && \
        minikube addons list | grep STATUS && minikube addons list | grep enabled && \
        printf '\n\n\033[4;33m Current status of Minikube \033[0m' && minikube status
fi