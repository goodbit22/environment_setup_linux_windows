#!/bin/bash
# shellcheck source=/dev/null
set -uo pipefail
if [ -f vagrant_install.sh ]; then                                                                            
     source vagrant_install.sh                                                                                   
else                                                                                                          
	echo -e "vagrant_install.sh file cannot be found$"                                        
    exit 1;                                                                                                     
 fi      

menu_run(){
	echo "1) ${BLUE}Uruchom wybrane maszyne zdefiniowane w Vagrantfile${WHITE}" 
	echo "2) ${BLUE}Uruchom wszystkie maszyny zdefiniowane w Vagrantfile${WHITE}"  
	echo "3) ${BLUE}Wroc${WHITE}" 
}

menu_delete(){
	echo "1) ${BLUE}Usun wybrane maszyne zdefiniowane w Vagrantfile${WHITE}" 
	echo "2) ${BLUE}Usun wszystkie maszyny zdefiniowane w Vagrantfile${WHITE}"  
	echo "3) ${BLUE}Wroc${WHITE}" 
}
 run_machines(){
    mapfile -t name_machines < <(cat < ../../../json_files/vagrant_machines.json | jq  '.[]["name"]')
	local options=("Uruchom wybrane maszyne zdefiniowane w Vagrantfile" "Uruchom wszystkie maszyny zdefiniowane w Vagrantfile" "Exit")
	PS3="Wybierz opcje: "
	echo "Wcisnij enter aby odswiezyc menu"
	select option in "${options[@]}" 
	do
		case $option in 
			"${options[0]}")
				echo -e "${CYAN}wybrales opcje $REPLY ${WHITE}"
				echo -e "${YELLOW}Lista dostepnych machine zdefiniowanych w plikach Vagrantfile${WHITE}"
				for name_machine in "${name_machines[@]}" 
            		do
	    				echo "$name_machine"
                	done
                read -rp "${CYAN}Jaki maszyne wirtualna chcesz postawic${WHITE} " user_machine
				for name_machine in "${name_machines[@]}" 
					do
                    	if [ "$user_machine"  == "$(echo "$name_machine" | tr -d '"')"  ]; then
							vagrant up "$user_machine"
							break
                        else
                            echo -e "${YELLOW}Nie zdefiniowano w plikach Vagrantfile: $user_machine ${WHITE}"
						fi
					done
                menu_run
					;;
			"${options[1]}")
				echo -e "${CYAN}wybrales opcje $REPLY ${WHITE}"
            	echo -e "${GREEN}stawianie maszyn wirtualnych zawartych w pliku Vagrantfile${WHITE}"
                vagrant up	
                menu_run								
            	;;
			"${options[2]}")
				echo -e "${CYAN}wybrales opcje $REPLY ${WHITE}"
                break								
            	;;               
			*) echo "nie ma takiej opcji $REPLY";;
		esac
	done
}


delete_machines(){
	local options=("Usun wybrane maszyne zdefiniowane w Vagrantfile" "Usun wszystkie maszyny zdefiniowane w Vagrantfile" "Wroc" )
	PS3="Wybierz opcje: "
	echo "Wcisnij enter aby odswiezyc menu"
	select option in "${options[@]}" 
	do
		case $option in 
			"${options[0]}")
				echo "${CYAN}wybrales opcje $REPLY ${WHITE}"
				vagrant destroy 
				menu_delete
				;;
			"${options[1]}")
				echo "${CYAN}wybrales opcje $REPLY ${WHITE}"
				echo "${GREEN}usuwanie maszyn zdefiniowanych w pliku  pliku Vagrantfile${WHITE}"
				vagrant destroy -f 
				menu_delete
				;;
			"${options[2]}")
				echo "${CYAN} wybrales opcje $REPLY ${WHITE}"
				break	
				;;
			*) echo "${YELLOW}invalid option$REPLY ${WHITE}";;
		esac
	done
}

main_function(){
	local options=("${BLUE}Run virtual machines${WHITE}" "${BLUE}Remove virtual machines${WHITE}" "${BLUE}QUIT${WHITE}")
	PS3="${CYAN}Select option: ${WHITE}"
	clear
	vagrant_install
	mapfile -t name_machines < <(cat < machines_vagrant.json | jq  '.[]["name"]')
	COLUMNS=12
	select option in "${options[@]}" 
	do
		case $option in 
			"${options[0]}")
				echo "${CYAN}you have chosen $REPLY option ${WHITE}"
				run_machines
				menu
				;;
			"${options[1]}")
				echo "${CYAN}you have chosen $REPLY option ${WHITE}"
				delete_machines
				menu
				;;
			"${options[2]}")
				break	
				;;
			*) 
				clear
				COLUMNS=12
				menu
				echo "${YELLOW}invalid option$REPLY ${WHITE}"
				;;
		esac
	done
}

main(){
	vagrant_install
	run_machines
}

main
