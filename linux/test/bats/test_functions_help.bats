#!/usr/bin/env bats

setup(){
    source ../../main_dependencies/function_help.sh
}

@test "pass when Distribution Linux is ok"{
    run check_distribute_linux
    [ "$status" -eq 0 ]
}

@test "pass when docker-compose is ok"{
    run docker-compose_test
    [ "$status" -eq 0 ]
}

@test "pass when docker is ok"{
    run docker_test
    [ "$status" -eq 0 ]
}