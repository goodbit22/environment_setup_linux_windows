#!/usr/bin/env bash
set -u
bats(){
    if ! bats -version &> '/dev/null'; then  
        local -r current="$PWD"
        git clone https://github.com/bats-core/bats-core.git && cd bats-core || exit 1 
        cd "$current" || exit 1 
        ./install '/usr/local' && rm bats-core
    fi
}
bats