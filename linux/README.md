# Skrypt do setup środowiska dla systemów Linux

## 1)Opis głównych skryptów

    * - oznaczenie waznych skryptow (które powinny byc uzyte do setup Linuxa)
    & - oznaczenie skryptów pobocznych
    ^ - oznaczenie skryptów zawierajace funkcje wykorzystywane w wielu skryptach (nie dotykac)

    * open_sites.sh - skrypt do automatycznego przejsc do stron zawierajacych konfiguracje poczty , pki i vpn. Skrypt dodatkowo otwiera strony ktoe mozna zapisac do ulubionych np tapir
    * main.sh - skrypt do instalacji wymaganego oprogramowania
    & init.sh - skrypt który dodaje uprawnienie wykonywania do pozostałych skryptów

## 2)Uruchamianie skryptów

Skrypty można uruchomić na dwa sposoby  
1.komendy:

* bash init.sh
* bash open_sites.sh.sh
* sudo bash main.sh

2.komendy:
    Nalezy przed wykonywaniem polecen uruchomic skrypt init.sh (bash init.sh)

* ./open_sites.sh.sh
* sudo ./main.sh
* sudo ./init.sh

## 3) Uwagi

* Jesli chcesz zeby skrypt automatycznie sciagnol obrazy dockerowe po zainstalowaniu wymaganego oprogramowania nalezy zmodyfikowac plik docker_images.sh
i dodac obrazy do pliku images_docker.json i.

W pliku docker_images.sh nalezy odkomentowac linie 14.
W pliku images_docker.json

```json
{
        "name" : "name_image",
        "tag" : "name_tag"
},
```

* Przed uruchomieniem skryptu --> install_generic_certs.sh nalezy wygenerować token nexus ,ponieważ porzebuje go do skonfigurowanie repozytorium nexus w docker

* Jesli skrypt do aktualizacji docker-compose wyswietli komunikat ze wersja jest aktualna nalezy sie upewnic i  wejsc na strone
<https://docs.docker.com/compose/install/> i zmienic wartosc w zmiennej latest_version jesli jest wyzsza

* Skrypt poprawnie sie uruchomi na dystrybucjach bazujacych na Debian, red hat(wersja testowa) i suse (wersja testowa).
* Jesli instalujesz Virtualbox podczas instalacji pakiet instalacyjny wlacza secure boot i kaze tobie ustawić haslo.  
Po instalacji nalezy  wylaczyc secure-boot
