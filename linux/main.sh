#!/bin/bash
set -ue

readonly path_dependency='main_dependencies' 
readonly scripts_main=('install_software.sh' 'function_help.sh' 'install_packages_suze.sh' 'install_packages_fedora.sh' 'install_packages_debian.sh')
for script in "${scripts_main[@]}"
do
	# shellcheck source=/dev/null
	if [ -f "${path_dependency}/${script}" ]; then
		source "${path_dependency}/${script}"
	else
		echo -e "Nie ma wymaganego pliku ${script}"
		exit 1;
	fi
done

readonly path_files='./main_dependencies/files'
readonly YamlJson_files=("cron/cronjob.txt")
for file in "${YamlJson_files[@]}"
do
	if ! [[ -e 	"${path_files}/${file}" ]]; then
		echo -e "${RED}Nie ma wymaganego pliku ${file}${WHITE}"
		exit 1;
	fi
done


main_program(){
	authorization
	check_network_connection
	main_install
	echo -e "${GREEN}Wszystkie operacje zostaly wykonane${WHITE}"
	echo -e "${MAGENTA}Jesli chcesz zapoznac sie z opcjonalnym programami uruchom skrypt software_optional.sh${WHITE}"
}
install_packages_nixos(){
    sh <(curl -L https://nixos.org/nix/install) --daemon
}

main_program
#install_packages_nixos