#linters python
pylint
flake8
mypy
bandit
pydocstyle

#linters ansible
ansible-lint

#scan python
pip-audit

#test python
pytest

#test anasible
ansible
molecule