#!/usr/bin/env python3
from json import load
from subprocess import  call

def read_file(filename='vsc_plugins.json') -> dict:
    """
    Function that returns dictionary from plugins
    """
    try:
        with open(filename, 'r',  encoding='UTF-8') as json_file:
            data = load(json_file)
    except FileNotFoundError:
        print(f"{filename} file not found")
    except IOError:
        print(f"Error reading file {filename}")
    return {name['name']: name['ext'] for name in data}

def install_plugins() -> None:
    """
    Function installs plugins
    """
    new = read_file()
    for _, value in new.items():
        call(['code', '--install-extension', value])
        print("Plugin has been installed")

if __name__ == '__main__':
    install_plugins()
    