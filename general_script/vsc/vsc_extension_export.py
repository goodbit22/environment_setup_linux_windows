#!/usr/bin/env python3
from subprocess import check_output
from json import dumps

def save_plugins() -> tuple[list[str], list[str]]:
    """
        Function that returns the names of plugins and exts
    """
    exts, plug_in_names = [], []
    plugins = check_output(['code', '--list-extensions'], encoding='UTF-8', text=True)
    for plugin in str(plugins).split('\n'):
        if plugin:
            plug_in_names.append(plugin.split(".")[1])
            exts.append(plugin)
    return plug_in_names, exts


def generate_json_file(filename='vsc_plugins.json') -> None:
    """
        Function that generates json file
    """
    plug_in_names, exts = save_plugins()
    data = [{'name': plug_in_name, 'ext': ext} for plug_in_name, ext in zip(plug_in_names, exts)]
    json_obj =  dumps(data, indent=2)
    try:
        with open(filename, "w", encoding='utf-8') as outfile:
            outfile.write(json_obj)
    except IOError:
        print("Error writing file {filename}")

if __name__ == '__main__':
    generate_json_file()
