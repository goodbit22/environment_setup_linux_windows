from subprocess import PIPE, Popen
from zipfile import ZipFile,  ZIP_DEFLATED, BadZipFile
from os import path, walk
from re import match

def unzip_file(file_path, destination) -> None:
    '''
    the function unpack archive
    '''
    try:
        with ZipFile(file_path, 'r') as zip_ref:
            zip_ref.extractall(destination)
            print('File unzipped successfully.')
    except FileNotFoundError:
        print('File not found.')
    except BadZipFile:
        print('Invalid zip file.')

def zip_directory(directory, output_zip) -> None:
    '''
    the function create archive
    '''
    zipf = ZipFile(output_zip, 'w', ZIP_DEFLATED)
    for root, dirs, files in walk(directory):
        for file in files:
            file_path = path.join(root, file)
            zipf.write(file_path, path.relpath(file_path, directory))
    zipf.close()

def get_pycharm_version() -> str:
    '''
    the function return version of pycharm
    '''
    process = Popen(
        ["pycharm-community", "--version"],
        stdout=PIPE,
        stderr=PIPE,
        text=True,
    )
    output, errors = process.communicate()
    if errors:
        print('command not found')
        exit(1)
    else:
        pycharm_version = [tag for out in output.split("\n") if match("PyCharm", out) for tag in out.split(" ")]
        name, version = pycharm_version[0], pycharm_version[1]
        version_year, version_month = version.split('.')[0] , '.'+ version.split('.')[1]
        subscription =  "CE" if pycharm_version[2]  == "(Community" else "P"
        print("".join([name, subscription, version_year, version_month ]))
        return "".join([name, subscription, version_year, version_month ])

def get_intellij_version() -> str:
    '''
    the function return version of Intellij
    '''
    process = Popen(
        ["intellij-idea-community", "--version"],
        stdout=PIPE,
        stderr=PIPE,
        text=True,
    )
    output, errors = process.communicate()
    if errors:
        print('command not found')
        exit(1)
    else:
        intellij_version = [tag for out in output.split("\n") if match("IntelliJ", out) for tag in out.split(" ")]
        name, version_year, version_month = intellij_version[1].capitalize(), intellij_version[2].split('.')[0] , '.'+ intellij_version[2].split('.')[1]
        subscription =  "IC" if intellij_version[3]  == "(Community" else "IP"
        print("".join([name, subscription, version_year,  version_month]))
        return "".join([name, subscription, version_year,  version_month])
