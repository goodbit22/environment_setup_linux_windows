#!/usr/bin/env python3
from sys import path
import os 
path.insert(0, '..')
from jetbrain import unzip_file, get_pycharm_version
 
def import_plugins():
    '''
    '''
    directory_version = get_pycharm_version()
    zip_file = 'backup.zip'
    destination_folder = os.path.expanduser('~') + "/.config/JetBrains/" + directory_version
    print(destination_folder)
    unzip_file(zip_file, destination_folder)

if __name__ == '__main__':
    import_plugins()