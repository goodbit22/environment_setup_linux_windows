#!/usr/bin/env python3
from sys import path
path.insert(0, '..')
from jetbrain import zip_directory, get_intellij_version


def export_plugins():
    '''
    '''
    directory_version = get_intellij_version()
    directory_to_zip = "~/.config/JetBrains/" + directory_version
    output_zip_file='backup.zip'
    zip_directory(directory_to_zip, output_zip_file)

if __name__ == "__main__":
    export_plugins()