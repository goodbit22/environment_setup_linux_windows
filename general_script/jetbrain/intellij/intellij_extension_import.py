#!/usr/bin/env python3
from sys import path
path.insert(0, '..')
from jetbrain import unzip_file, get_intellij_version

def import_plugins():
    '''
    '''
    directory_version = get_intellij_version()
    zip_file = 'backup.zip'
    destination_folder = "~/.config/JetBrains/" + directory_version
    unzip_file(zip_file, destination_folder)

if __name__ == '__main__':
    import_plugins()