image: python:slim
stages:
  - "lint"
  - "scan"
  - "test"

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/pip-cache"

cache:
  paths:
  - "$CI_PROJECT_DIR/pip-cache"
  key: "$CI_PROJECT_ID"


before_script:
  - pip install -r requirements-ci.txt 

static analysis python 1/5:
  stage: lint
  script:
    - pylint general_script
  allow_failure: true

static analysis python 2/5:
  stage: lint 
  script:
    - flake8 general_script
  allow_failure: true

static analysis python 3/5:
  stage: lint
  script:
    - bandit general_script
  allow_failure: true

static analysis python 4/5:
  stage: lint
  script:
    - mypy general_script
  allow_failure: true

static analysis python 5/5:
  stage: lint
  script:
    - pydocstyle general_script
  allow_failure: true


static analysis yaml:
  stage: lint
  before_script:
    - pip install --user yamllint 
  script:
    - echo 'test'
  allow_failure: true

static analysis ansible_playbook: 
  stage: lint
  script: 
    - ansible-lint linux/main_dependencies/playbooks/*.yml
  allow_failure: true 

static analysis bash:
  stage: lint
  variables:
    "tag": "latest"
  image: "registry.gitlab.com/pipeline-components/shellcheck:${tag}"
  before_script:
    - bash --version
  script:
    - shellcheck linux/*.sh 
    - shellcheck linux/main_dependencies/*.sh 
    - shellcheck linux/scripts/{audit,docker,shell,webbrowsers}/*.sh 
  allow_failure: true

static analysis powershell:
  stage: lint
  variables:
    "tag": "lts-debian-bullseye-slim-20220318"
  image: "mcr.microsoft.com/powershell:${tag}"
  before_script:
    - pwsh -Version
    - pwsh -Command  "Set-PSRepository -ErrorAction Stop  -InstallationPolicy Trusted -Name PSGallery  -Verbose"
    - pwsh -Command "Install-Module   -ErrorAction Stop  -Name PSScriptAnalyzer  -RequiredVersion 1.21.0   -Verbose"
  script:
    - pwsh -Command "Invoke-ScriptAnalyzer -EnableExit -Recurse -Path windows"
    - pwsh -Command "Invoke-ScriptAnalyzer -EnableExit -Recurse -Path windows/script/docker"
    - pwsh -Command "Invoke-ScriptAnalyzer -EnableExit -Recurse -Path windows/script/powershell"
  allow_failure: true

static analysis markdown:
  stage: lint
  variables:
    "tag": "latest"
  image: "registry.gitlab.com/pipeline-components/markdownlint-cli2:${tag}"
  script:
    - markdownlint-cli2 . 
    - markdownlint-cli2 linux
    - markdownlint-cli2 windows

  allow_failure: true

semgrep:
  stage: "scan"
  variables:
    "tag": "latest"
  image: "semgrep/semgrep:${tag}"
  before_script:
    semgrep --help
  script: 
    - semgrep login
    - semgrep ci
    - semgrep --config "p/docker-compose"
    - semgrep --config "p/python"
    - semgrep --config "p/dockerfile"
  allow_failure: true

pip-audit:
  stage: scan
  allow_failure: true
  script: pip-audit requirements-ci.txt


python_tests:
  stage: test
  before_script:
    pip install pytest
  script:
    - echo 'test'

bash_tests:
  stage: test
  variables:
    "tag": "latest"
  image: "bats/bats:${tag}"
  before_script:
    - bats --version
  script: 
    - echo 'test'

powershell_tests:
  stage: test
  variables:
    "tag": "lts-debian-bullseye-slim-20220318"
  image: "mcr.microsoft.com/powershell:${tag}"
  before_script:
    - pwsh -Command "Install-Module   -ErrorAction Stop  -Name Pester -Verbose"  
  script: 
    - echo "test"

ansible_tests 1/4:
  stage: test
  allow_failure: true
  before_script:
    - ansible-test --version  
  script: 
    - ansible-test units

ansible_tests 2/4:
  stage: test
  allow_failure: true
  before_script:
    - ansible-test --version  
  script: 
    - ansible-test integration  

ansible_tests 3/4:
  stage: test
  allow_failure: true
  before_script:
    - ansible-playbook  --version  
  script: 
    - ansible-playbook --syntax-check ./linux/main_dependencies/playbooks/start_linux_config.yml 
    - ansible-playbook --syntax-check ./linux/main_dependencies/playbooks/start_win_config.yml
    - ansible-playbook --syntax-check ./linux/main_dependencies/playbooks/secure.yml

ansible_tests 4/4:
  stage: test
  allow_failure: true
  before_script:
    - ansible-playbook -v
  script: 
    - ansible-playbook ./linux/main_dependencies/playbooks/start_linux_config.yml --check   
    - ansible-playbook ./linux/main_dependencies/playbooks/start_win_config.yml --check 
    - ansible-playbook ./linux/main_dependencies/playbooks/secure.yml --check 
