# environment_setup_linux_windows

Skrypty do automatyzacji setup windows i linux

## Wymagania

* Powershell (Windows)
* Bash (linux)

## Uwagi

* Wszystkie skrypty na Windows trzeba odpalac na uprawnieniach administratora
* Na poczatku nalezy uruchomic skrypt update_powershell.ps1 ( w celu aktualizacji powershell)
* <https://superuser.com/questions/106360/how-to-enable-execution-of-powershell-scripts> (Trzeba ustawiać w powershell polityki ponieważ domyślne jest zablokowane uruchamianie skryptów powershell w Windows)
