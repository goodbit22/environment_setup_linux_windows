Set-Variable -Name PathModules -Value "./main_dependencies/PSModule" -Option Constant
$modules=('UpdatePowershell/UpdatePowershell.psd1', 'Functions/Functions.psd1')
foreach($module in $modules){
    if(Test-Path "$PathModules/$module"){
        Import-Module -Name "$PathModules/$module"
    }
    else{
        Write-Error "Brak modulu -> $module" 
        Exit 1
    } 
}

function services_disable(){
    $services=@{'Telnet Client'='TelnetClient'; 'Net.TCP Port Sharing'='WCF-TCP-PortSharing45'; ' Windows Fax and Scan'='FaxServicesClientPackage'; 
    'Windows Media Player'='WindowsMediaPlayer'; 'SMB Direct'='SmbDirect'; 'TFTP Client'='TFTP'; 'XPS Services'='Printing-XPSServices-Features'}
    foreach($service in $services.Keys){
        $service_exists = Get-Service -Name $service -ErrorAction SilentlyContinue;
        if($service_exists) {
            Write-ColorOutput "Disable $service feature"  -ForegroundColor 'Green'
            dism /Online /Disable-Feature /FeatureName: "${services[$service]}" /NoRestart
        }
    }
}
function onedrive_disable(){ 
    $process=Get-Process -Name 'OneDrive' -ErrorAction SilentlyContinue;
    if ($?){
        taskkill /f /im OneDrive.exe
        Write-ColorOutput 'Kill OneDrive process' -ForegroundColor 'Green'
    }
    $leftovers=("$env:UserProfile\OneDrive", "$env:LocalAppData\Microsoft\OneDrive", "$env:ProgramData\Microsoft OneDrive", "$env:SystemDrive\OneDriveTemp")
    foreach( $leftover in $leftovers){
        $exists = Test-Path $leftover
        if ($exists -eq 'True'){
            Remove-Item -Path $leftover -Force -Recurse
            Write-ColorOutput "--- Remove OneDrive leftovers ($leftover)" -ForegroundColor 'Green'
        }
    }
    $shortcuts=("$env:APPDATA\Microsoft\Windows\Start Menu\Programs\Microsoft OneDrive.lnk", "$env:APPDATA\Microsoft\Windows\Start Menu\Programs\OneDrive.lnk", "$env:USERPROFILE\Links\OneDrive.lnk")
    foreach($shortcut in $shortcuts){
        $exists = Test-Path $shortcut
        if ($exists -eq 'True'){
            Remove-Item -Path $shortcut -Force -Recurse
            Write-ColorOutput "--- Delete OneDrive shortcuts ($shortcut)" -ForegroundColor 'Green'
        }
    }
    $register_keys=@{"DisableFileSyncNGSC"="HKLM\SOFTWARE\Policies\Microsoft\Windows\OneDrive"; "DisableFileSync"="HKLM\SOFTWARE\Policies\Microsoft\Windows\OneDrive";}
    foreach($property in $register_keys.Keys){
        $st = check_registry_entries $register_keys[$property] $property
        if($st -eq $false){
            Write-ColorOutput "--- Disable usage of OneDrive -> $property" -ForegroundColor 'Green'
            reg add $register_keys[$property]  /t REG_DWORD /v $property /d 1 /f
        }
    }
}

function remove_default_app(){
    $apps_windows=@{'Cortana'='Microsoft.549981C3F5F10'; 'Get Help'='Microsoft.GetHelp'; 'Tips'='Microsoft.Getstarted';
    'Messaging'='Microsoft.Messaging'; 'Mixed Reality Portal'='Microsoft.MixedReality.Portal'; 'Feedback Hub'='Microsoft.WindowsFeedbackHub'; 
    'Windows Alarms and Clock'='Microsoft.WindowsAlarms'; 'Windows Camera'='Microsoft.WindowsCamera'; 'Paint 3D '='Microsoft.MSPaint'; 
     'Windows Maps'='Microsoft.WindowsMaps'; 'Minecraft for Windows 10'='Microsoft.MinecraftUWP'; 'Microsoft People'='Microsoft.People'; 
     'Microsoft Pay'='Microsoft.Wallet'; 'Store Purchase'='Microsoft.StorePurchaseApp'; 'Snip ^& Sketch'='Microsoft.ScreenSketch'; 
     'Print 3D'='Microsoft.Print3D'; 'Mobile Plans'='Microsoft.OneConnect'; 'Microsoft Solitaire Collection'='Microsoft.MicrosoftSolitaireCollection'; 
     'Microsoft Sticky Notes'='Microsoft.MicrosoftStickyNotes'; 'Mail and Calendar'='microsoft.windowscommunicationsapps';
     'Windows Calculator'='Microsoft.WindowsCalculator'; 'Microsoft Photos'='Microsoft.Windows.Photos'; 'Skype'='Microsoft.SkypeApp'; 
     'Microsoft Store'='Microsoft.WindowsStore'; 'GroupMe'='Microsoft.GroupMe10'; 'Windows Voice Recorder'='Microsoft.WindowsSoundRecorder';
     'Microsoft 3D Builder'='Microsoft.3DBuilder'; '3D Viewer'='Microsoft.Microsoft3DViewer'; 'MSN Weather'='Microsoft.BingWeather';
    'MSN Sports'='Microsoft.BingSports'; 'MSN News'='Microsoft.BingNews'; 'MSN Money'='Microsoft.BingFinance';
    'My Office'='Microsoft.MicrosoftOfficeHub'; 'OneNote'='Microsoft.Office.OneNote'; 'Xbox Console Companion'='Microsoft.XboxApp'; 
    'Xbox Game Bar'='Microsoft.XboxGamingOverlay'; 'Groove Music'='Microsoft.ZuneMusic'; 'Movies and TV'='Microsoft.ZuneVideo';
    'Twitter'='9E2F88E3.Twitter'; 'Candy Crush Saga'='king.com.CandyCrushSaga'; 'Candy'='king.com.CandyCrushSodaSaga'; 'Contact Support'='Windows.ContactSupport';
    'Windows Print 3D'='Windows.Print3D'
    }
    foreach($app in $apps_windows.Keys){
        Write-ColorOutput "Unnistall $app app ..."
        $package=Get-AppxPackage  -AllUsers  -Name  $apps_windows[$app]
        if (!$package){
            Write-ColorOutput "Not installed ($app)" -ForegroundColor 'Blue'
        }
        else{
            Get-AppxPackage  -AllUsers  -Name  $apps_windows[$app] | Remove-AppxPackage -AllUsers
            if ($?){
                Write-ColorOutput "Unnistalled ($app)" -ForegroundColor 'Green'
            }
        }
    }
}

function disable_feature (){
    $features_windows=@{'Direct Play'="DirectPlay"; 'Legacy Components'="LegacyComponents"; 'Media Feature'="MediaPlayback"; 
    'Scan Management'="ScanManagementConsole"; 'Internet Printing Client'='Printing-Foundation-InternetPrinting-Client';
    'LPD Print'="LPDPrintService"}
    foreach($feature in $features_windows.Keys){
        $feature_exists = Get-Service -Name $feature -ErrorAction SilentlyContinue;
        if($feature_exists) {
            Write-ColorOutput "Disable $feature feature"
            dism /Online /Disable-Feature /FeatureName:"${features_windows[$feature]}" /NoRestart
        }
    } 
}
function main(){
    check_version
    services_disable
    onedrive_disable
    remove_default_app
    disable_feature
}
main