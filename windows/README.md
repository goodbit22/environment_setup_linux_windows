# Skrypt do setup środowiska dla systemów Windows

## 1)Opisz głównych skryptów

    * - oznaczenie waznych skryptow (które powinny byc uzyte do setup Windows)
    & - oznaczenie skryptów pobocznych

    * update_powershell.ps1 - skrypt instalacyjny powershell7
    * env_setup_tool.ps1 - głowny skrypt do instalacji programow
    & open_sites.ps1 - skrypt otwierajacych wiele waznych stron 
    & clean_start.ps1 -  usuwa zbedne aplikacje windows
    & secure.ps1 - skrypt do hardeningu systemu operacyjnego 
    & open_sites.ps1 - skrypt otwierajacych wiele waznych stron 

## 2)Uruchamianie skryptow

-komendy:

* .\update_powershell.ps1
* .\env_setup_tool.ps1
* .\open_sites.ps1
* .\clean_start.ps1
* .\secure.ps1

## 3) Uwagi

* <https://superuser.com/questions/106360/how-to-enable-execution-of-powershell-scripts> (Trzeba ustawiać w powershell polityki ponieważ domyślne jest zablokowane uruchamianie skryptów powershell w Windows)
