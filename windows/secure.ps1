Set-Variable -Name PathModules -Value "./main_dependencies/PSModule" -Option Constant
$modules=('UpdatePowershell/UpdatePowershell.psd1', 'Functions/Functions.psd1')
foreach($module in $modules){
    if(Test-Path "$PathModules/$module"){
        Import-Module -Name "$PathModules/$module"
    }
    else{
        Write-Error  "Brak modulu -> $module" -ForegroundColor 'Red'
        Exit 1
    } 
}

function disable_unsafe_service(){  
    $features=('SMB1Protocol', 'SMB1Protocol-Client', 'SMB1Protocol-Server', "MicrosoftWindowsPowerShellV2Root", "MicrosoftWindowsPowerShellV2",
    "Internet-Explorer-Optional-x64", "Internet-Explorer-Optional-x84", "Internet-Explorer-Optional-amd64")
    foreach ($feature in $features) {
        $state = Get-WindowsOptionalFeature -Online -FeatureName "$feature" | Select-object -ExpandProperty State
        if ($state -eq 'Enabled'){
            if ($features -match 'SMB1Protocol*'){
                Write-ColorOutput  "--- Disable unsafe SMBv1 protocol -> ($feature)" -ForegroundColor 'Green'
                dism /online /Disable-Feature /FeatureName:"$feature" /NoRestart
            }
            elseif ($features -match 'MicrosoftWindowsPowerShell*') {
                Write-ColorOutput  "--- Disable PowerShell 2.0 against downgrade attacks -> ($feature)"   -ForegroundColor 'Green'
                dism /online /Disable-Feature /FeatureName:"$feature"  /NoRestart    
            }
            elseif ($features -match 'Internet-Explorer-Optional-*') {
                Write-ColorOutput  "--- Internet Explorer feature disable -> ($feature)" -ForegroundColor 'Green'
                dism /online /Disable-Feature /FeatureName:"$feature"  /NoRestart
            }
        }
        else {
            Write-ColorOutput  "--- Unsafe service was disabled earlier -> ($feature)" -ForegroundColor 'Blue'
        } 
    }
}

function secure_system(){
    $ErrorActionPreference = 'SilentlyContinue'
    Write-ColorOutput  "`r"
    Write-ColorOutput  "--- Disable administrative shares" -ForegroundColor 'Blue'
    Write-ColorOutput  "--- Prevent WinRM from using Basic Authentication" -ForegroundColor 'Blue'
    Write-ColorOutput  "--- Restrict anonymous access to Named Pipes and Shares" -ForegroundColor 'Blue'
    Write-ColorOutput  "--- Force enable data execution prevention (DEP)" -ForegroundColor 'Blue'
    Write-ColorOutput  "--- Enable Structured Exception Handling Overwrite Protection (SEHOP)" -ForegroundColor 'Blue'
    Write-ColorOutput  "--- Disable WCN Registrars" -ForegroundColor 'Blue'
    $register_keys_0=@{"AutoShareWks"="HKLM\SYSTEM\CurrentControlSet\Services\LanmanServer\Parameters"; "AllowBasic"="HKLM\SOFTWARE\Policies\Microsoft\Windows\WinRM\Client";
    "fAllowToGetHelp"="HKLM\SYSTEM\CurrentControlSet\Control\Remote Assistance"; "fAllowFullControl"="HKLM\SYSTEM\CurrentControlSet\Control\Remote Assistance";
    "NoDataExecutionPrevention"="HKLM\SOFTWARE\Policies\Microsoft\Windows\Explorer";  "DisableHHDEP"="HKLM\SOFTWARE\Policies\Microsoft\Windows\System";
    "DisableExceptionChainValidation"="HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\kernel"; "EnableRegistrars"="HKLM\SOFTWARE\Policies\Microsoft\Windows\WCN\Registrars";
    "DisableFlashConfigRegistrar"="HKLM\SOFTWARE\Policies\Microsoft\Windows\WCN\Registrar"; "DisableWPDRegistrar"="HKLM\SOFTWARE\Policies\Microsoft\Windows\WCN\Registrars";
    "DisableInBand802DOT11Registrar"="HKLM\SOFTWARE\Policies\Microsoft\Windows\WCN\Registrars"; "DisableUPnPRegistrar"="HKLM\SOFTWARE\Policies\Microsoft\Windows\WCN\Registrars" }
    foreach($property in $register_keys_0.Keys){
        $st = check_registry_entries $register_keys_0[$property] $property
        if($st -eq $false){
            reg add $register_keys_0[$property] /v $property /t REG_DWORD /d 0 /f
        }
    }    
    Write-ColorOutput  "--- Block Anonymous enumeration of SAM accounts" -ForegroundColor 'Blue'
    Write-ColorOutput  "--- Restrict anonymous access to Named Pipes and Shares" -ForegroundColor 'Blue'
    Write-ColorOutput  "--- Disable the Windows Connect Now wizard" -ForegroundColor 'Blue'
    Write-ColorOutput  "--- Prevent the storage of the LAN Manager hash of passwords" -ForegroundColor 'Blue'
    Write-ColorOutput  "--- Restrict anonymous enumeration of shares" -ForegroundColor 'Blue'
    Write-ColorOutput  "--- Refuse less secure authentication" -ForegroundColor 'Blue'
    $register_keys_1=@{"RestrictAnonymous"="HKLM\SYSTEM\CurrentControlSet\Control\LSA";  "NoLMHash"="HKLM\SYSTEM\CurrentControlSet\Control\Lsa";
    "DisableWcnUi"="HKLM\Software\Policies\Microsoft\Windows\WCN\UI"; "RestrictNullSessAccess"="HKLM\SYSTEM\CurrentControlSet\Services\LanManServer\Parameters";
    "RestrictAnonymousSAM"="HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\kernel"; "LmCompatibilityLevel"="HKLM\SYSTEM\CurrentControlSet\Control\Lsa"}
    foreach($property in $register_keys_1.Keys){
        $st = check_registry_entries $register_keys_1[$property] $property
        if($st -eq $false){
            if ($property -eq "LmCompatibilityLevel"){
                reg add "HKLM\SYSTEM\CurrentControlSet\Control\Lsa" /v "LmCompatibilityLevel" /t REG_DWORD /d 5 /f
            }
            else{
                reg add $register_keys_1[$property] /v $property /t REG_DWORD /d 1 /f
            } 
        }
    }
    $register_keys_net=("HKLM\SOFTWARE\Microsoft\.NETFramework\v2.0.50727", "HKLM\SOFTWARE\WOW6432Node\Microsoft\.NETFramework\v2.0.50727",
    "HKLM\SOFTWARE\Microsoft\.NETFramework\v3.0", "HKLM\SOFTWARE\WOW6432Node\Microsoft\.NETFramework\v3.0", "HKLM\SOFTWARE\Microsoft\.NETFramework\v4.0.30319",
    "HKLM\SOFTWARE\WOW6432Node\Microsoft\.NETFramework\v4.0.30319")
    $properties_net=('SchUseStrongCrypto', 'SystemDefaultTlsVersions')
    Write-ColorOutput  "--- Enabling Strong Authentication for .NET applications (TLS 1.2)" -ForegroundColor 'Blue'
    foreach($register_key_net in $register_keys_net){
        foreach($property_net in $properties_net){
            $st = check_registry_entries $register_key_net $property_net
            if ( $st -eq $false){
                reg add "$register_key_net" /f /v "$property_net" /t REG_DWORD /d 0x00000001
                Write-ColorOutput   "-- Add ($property_net) entry to  ($register_key_net) register key" -ForegroundColor 'Magenta'
            }
        }
    }
    Write-ColorOutput  "--- Force not to respond to renegotiation requests" -ForegroundColor 'Blue'
    $register_keys_schannel=@{'DisableRenegoOnServer'="HKLM\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL";
    'UseScsvForTls'="HKLM\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL"; 'AllowInsecureRenegoClients'="HKLM\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL";
    'AllowInsecureRenegoServer'="HKLM\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL"}
    foreach($property in $register_keys_schannel.Keys){
        $st = check_registry_entries $register_keys_schannel[$property] $property
        if($st -eq $false){
            if ($property -eq "DisableRenegoOnServer" -or $property -eq "UseScsvForTls" ){
                reg add $register_keys_schannel[$property] /f /v $property /t REG_DWORD /d 0x00000001
            }
            else{
                reg add $register_keys_schannel[$property] /f /v $property /t REG_DWORD /d 0x00000000
            }
        }
    }
}

function secure_vulnerability_spectre(){
    $ErrorActionPreference = 'SilentlyContinue'
    Write-ColorOutput  "`r"
    Write-ColorOutput  "--- Spectre variant 2 and meltdown (own OS)" -ForegroundColor 'Blue'
    $st = check_registry_entries "HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Memory Management" "FeatureSettingsOverrideMask"
    if($st -eq $false){
        reg add "HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Memory Management" /v "FeatureSettingsOverrideMask" /t REG_DWORD /d 3 /f
    }    
    wmic cpu get name | findstr "Intel" >nul && (
        check_registry_entries "HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Memory Management" "FeatureSettingsOverride" && (
            reg add "HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Memory Management" /v "FeatureSettingsOverride" /t REG_DWORD /d 0 /f
        )
    )
    wmic cpu get name | findstr "AMD" >nul && (
        check_registry_entries "HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Memory Management" "FeatureSettingsOverride" && (
            reg add "HKLM\SYSTEM\CurrentControlSet\Control\Session Manager\Memory Management" /v "FeatureSettingsOverride" /t REG_DWORD /d 64 /f
        )
    )
}

function disable_algorithm_old(){
    #$ErrorActionPreference = 'SilentlyContinue'
    $ciphers=("RC2 40/128", "RC2 56/128", "RC2 128/128", "RC4 128/128", "RC4 64/128", "RC4 56/128", "RC4 40/128",
    "DES 56", "DES 56/56", "Triple DES 168", "Triple DES 168/168")
    foreach($cipher in $ciphers){
        Write-ColorOutput  "--- Disable $cipher cipher" -ForegroundColor 'Blue'
        $st = check_registry_entries "HKLM\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\$cipher" "Enabled"
        if($st -eq $false){
            reg add "HKLM\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\$cipher" /f /v Enabled /t REG_DWORD /d 0x00000000
        }
    }
    $hash_functions=('MD5', 'SHA', 'NULL', 'DTLS 1.0', 'SSLv2', 'TLS 1.0', 'TLS 1.1', 'SSLv3', "DTLS 1.1")
    $types=("Server Enabled", "Server DisabledByDefault", "Client Enabled", "Client DisabledByDefault")
    foreach($hash_function in $hash_functions){
        Write-ColorOutput  "--- Disable $hash_function" -ForegroundColor 'Blue'
        if ($hash_function -eq 'MD5' -or $hash_function -eq 'SHA' ) {
            $st = check_registry_entries "HKLM\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Hashes\$hash_function"  "Enabled"
            if($st -eq $false){
                reg add "HKLM\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Hashes\$hash_function" /f /v Enabled /t REG_DWORD /d 0x00000000
            }
        }
        elseif($hash_function -eq  'NULL'){
            $st = check_registry_entries "HKLM\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\$hash_function" "Enabled"
            if($st -eq $false){
                reg add "HKLM\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Ciphers\$hash_function" /f /v Enabled /t REG_DWORD /d 0x00000000
            }
        }
        else {
            foreach($type in $types){
                $kind=$type.split(" ")[0]
                $key=$type.split(" ")[1]
                if($key -eq 'Enabled'){
                    $st = check_registry_entries "HKLM\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\$hash_function\$kind"  "$key"
                    if($st -eq $false){
                        reg add "HKLM\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\$hash_function\$kind" /f /v $key /t REG_DWORD /d 0x00000000
                    }
                }
                else{
                    $st = check_registry_entries "HKLM\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\$hash_function\$kind"  "$key"
                    if($st -eq $false){
                        reg add "HKLM\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\$hash_function\$kind" /f /v $key /t REG_DWORD /d 0x00000001
                    }
                }
            }
        }
    }   
    $net_registries=("HKLM\SOFTWARE\Microsoft\.NETFramework\v2.0.50727","HKLM\SOFTWARE\WOW6432Node\Microsoft\.NETFramework\v2.0.50727", 
    "HKLM\SOFTWARE\Microsoft\.NETFramework\v3.0", "HKLM\SOFTWARE\WOW6432Node\Microsoft\.NETFramework\v3.0", "HKLM\SOFTWARE\Microsoft\.NETFramework\v4.0.30319",
    "HKLM\SOFTWARE\WOW6432Node\Microsoft\.NETFramework\v4.0.30319")
    $net_properties=("SchUseStrongCrypto", "SystemDefaultTlsVersions")
    foreach($net_registry in $net_registries){
        foreach( $net_property in $net_properties){
            $st = check_registry_entries $net_registry $net_property
            if($st -eq $false){
                reg add "$net_registry" /f /v "$net_property" /t REG_DWORD /d 0x00000001
            }
        }
    }
}

function enable_algorithm(){
    $ErrorActionPreference = 'SilentlyContinue'
    $protocols=("TLS 1.3", "DTLS 1.3")
    $types=("Server", "Client")
    $properties=("Enabled", "DisabledByDefault")
    foreach ($protocol in  $protocols){
        Write-ColorOutput  "--- Enable $protocol" -ForegroundColor 'Blue'
        foreach ($type in $types){
              foreach ($property in $properties){
                $st = check_registry_entries "HKLM\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\$protocol\$type" $property
                if($st -eq $false){
                    if ($property -eq "Enabled"){
                        reg add "HKLM\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\$protocol\$type" /f /v Enabled /t REG_DWORD /d 0x00000001
                    }
                    else {
                        reg add "HKLM\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\$protocol\$type" /f /v DisabledByDefault /t REG_DWORD /d 0x00000000
                    }
                }
              }  
        }
    }
}

function disable_functions(){
    $ErrorActionPreference = 'SilentlyContinue'
    Write-ColorOutput  "`r"
    Write-ColorOutput  "--- Disable AutoPlay and AutoRun" -ForegroundColor 'Blue'
    Write-ColorOutput  "--- Disable Windows Installer Always install with elevated privileges" -ForegroundColor 'Blue'
    $functions=@{"NoDriveTypeAutoRun"="HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer"; "NoAutorun"="HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer";
    "NoAutoplayfornonVolume"="HKLM\SOFTWARE\Policies\Microsoft\Windows\Explorer"; "AlwaysInstallElevated"="HKLM\SOFTWARE\Policies\Microsoft\Windows\Installer"}
    foreach($function in $functions.Keys){
        $st = check_registry_entries $functions[$function] $function
        if($st -eq $false){
            if ($function -eq "NoDriveTypeAutoRun"){
                reg add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer" /v "NoDriveTypeAutoRun" /t REG_DWORD /d 255 /f 
            }
            elseif ($features -eq "NoAutorun") {
                reg add "HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer" /v "NoAutorun" /t REG_DWORD /d 1 /f
            }
            elseif ($features -eq "NoAutoplayfornonVolume") {
                reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\Explorer" /v "NoAutoplayfornonVolume" /t REG_DWORD /d 1 /f
            }
            elseif ($features -eq "AlwaysInstallElevated") {
                reg add "HKLM\SOFTWARE\Policies\Microsoft\Windows\Installer" /v "AlwaysInstallElevated" /t REG_DWORD /d 0 /f
            }
        }
    }
}

function main(){
    check_version
    disable_unsafe_service
    secure_system
    secure_vulnerability_spectre
    disable_algorithm_old
    enable_algorithm
    disable_functions
}
main