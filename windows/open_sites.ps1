Set-Variable -Name PathModules -Value "./main_dependencies/PSModule" -Option Constant
$modules=('UpdatePowershell/UpdatePowershell.psd1', 'Functions/Functions.psd1')
foreach($module in $modules){
    if(Test-Path "$PathModules/$module"){
        Import-Module -Name "$PathModules/$module"
    }
    else{
        Write-Error "Brak modulu -> $module"  'Red'
        Exit 1
    }
}

function display_sites(){
    $sites=("https://www.youtube.com", "https://myanimelist.net/", "https://github.com/goodbit22", "https://gitlab.com/goodbit22",
    "https://mega.nz/fm/QgxmEarZ", "https://mail.google.com", "https://cert.pl/posts/2020/03/ostrzezenia_phishing/")
    foreach($site in $sites){
        Write-ColorOutput "Otworzono strone: $site" -ForegroundColor 'Blue'
        Start-Process 'msedge.exe' -ArgumentList "-url $site"
    }
}

function add_extensions(){
	$extensions=("https://addons.mozilla.org/firefox/downloads/file/3953362/adblocker_ultimate-latest.xpi", 
"https://addons.mozilla.org/firefox/downloads/file/3655554/disconnect-latest.xpi",
"https://addons.mozilla.org/firefox/downloads/file/3954503/darkreader-latest.xpi",
"https://addons.mozilla.org/firefox/downloads/file/3956871/duckduckgo_for_firefox-latest.xpi",
"https://addons.mozilla.org/firefox/downloads/file/3923300/facebook_container-latest.xpi",
"https://addons.mozilla.org/firefox/downloads/file/3872283/privacy_badger17-latest.xpi")
    foreach($extension in $extensions) {
        $extension_package=split-path "$extension" -Leaf
        Invoke-WebRequest -Uri "${extensions}"
        Write-ColorOutput "Pobrano plugin $extension_package" "Green"
        firefox.exe  -silent -install-global-extension "$extension_package"
        Remove-Item "$extension_package"
    }
}

function main(){
	display_sites
	add_extensions
}
main
