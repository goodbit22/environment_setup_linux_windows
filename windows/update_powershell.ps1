Set-Variable -Name PathModules -Value "./main_dependencies/PSModule" -Option Constant
$modules=('UpdatePowershell/UpdatePowershell.psd1')
foreach($module in $modules){
    if(Test-Path "$PathModules/$module"){
        Import-Module -Name "$PathModules/$module" 
    }
    else{
        Write-Error "Brak modulu -> $module" -ForegroundColor 'Red'; Exit 1
    } 
}
check_version