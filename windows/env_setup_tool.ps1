Set-Variable -Name PathModules -Value "./main_dependencies/PSModule" -Option Constant
$modules=('UpdatePowershell/UpdatePowershell.psd1', 'Functions/Functions.psd1')
foreach($module in $modules){
    if(Test-Path "$PathModules/$module"){
        Import-Module -Name "$PathModules/$module"
    }
    else{
        Write-Error "Brak modulu -> $module"  'Red'
        Exit 1
    }
}


function return_default_software_version($default_software_versions, $name_software){
    if(!$null -eq $default_software_versions){
        foreach( $soft in $default_software_versions){
            if ($name_software -eq $soft.software ){
                return $soft.version;
            }
        }
    }
    else{
        return 0;
    }
}

function configuration_Hyper_V(){
    Install-WindowsFeature -Name Hyper-V, RSAT-Hyper-V-Tools
}

function install_discord(){
    url='https://discord.com/api/downloads/distributions/app/installers/latest?channel=stable&platform=win&arch=x86'
    $path_file_exe="C:/tmp/discord.exe"
    if (Test-Path $path_file_exe){
        Write-ColorOutput "plik exe discord został wczesniej pobrany"  'Yellow'
        return;
    }
    else{
        Write-ColorOutput "Zaczeto sciaganie pliku "  'Blue'
        [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls13
        try{
            Invoke-WebRequest -Uri "$url" -OutFile "$path_file_exe"
            Write-ColorOutput "Pobrano plik i rozpoczeto jego instalacje"  'Blue'
            Start-Process  "$path_file_exe"    -argumentlist "/passive /norestart" -wait
            Write-ColorOutput "zakonczono instalacje"  'Green'
        }
        catch{
            Write-Error "Problem z pobraniem discord"
        }
    }
}

function install_gradle(){
    $gradle_directory='C:/gradle'
    New-Item -Path  "${gradle_directory}" -Type Directory
    Invoke-WebRequest -Uri "https://services.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip"   -OutFile "C:/tmp/${GRADLE_VERSION}-bin.zip" -UseBasicParsing
    [System.IO.Compression.ZipFile]::ExtractToDirectory("$gradle_directory" , "C:/tmp/gradle-${GRADLE_VERSION}-bin.zip")
    Write-ColorOutput "Instalacja Gradle"
}

function install_python($default_software_versions){
    if (
        (Invoke-RestMethod 'https://www.python.org/downloads/') -notmatch
        '\bhref="(?<url>.+?\.exe)"\s*>\s*Download Python (?<version>\d+\.\d+\.\d+)'
    ) { throw "Could not determine latest Python version and download URL" }
    $latestVersion_python = = $Matches.url
    Write-ColorOutput "Latest version of Python available: $latestVersion_python"
    $version_python = 0
    while($version_python -eq 0){
        $version_python = Read-Host "Podaj wersje python: "
        if (!$version_python){
            Write-ColorOutput "Nie wprowadzono zadnej wersji python wiec zostanie wybrana wersja z pliku json"   "Yellow"
            $version_python =  return_default_software_version $default_software_versions "python"
        }
    }
    Write-ColorOutput "Wybrana wersja python $version_python"
    $url="https://www.python.org/ftp/python/$version_python/python-$version_python-amd64.exe"
    $path_file_exe="C:/tmp/python-$version_python-amd64.exe"
    if (Test-Path $path_file_exe){
        Write-ColorOutput "plik exe python został wczesniej pobrany"  'Yellow'
        return;
    }
    else{
        Write-ColorOutput "Zaczeto sciaganie pliku "  'Blue'
        [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls13
        try{
            Invoke-WebRequest -Uri "$url" -OutFile "$path_file_exe"
            Write-ColorOutput "Pobrano plik i rozpoczeto jego instalacje"  'Blue'
            Start-Process  "$path_file_exe"    -argumentlist "/passive /norestart" -wait
            Write-ColorOutput "zakonczono instalacje"  'Green'
        }
        catch{
            Write-ColorOutput "podana przez ciebie wersja python nie istnieje"   Red
        }
    }
}

function install_minikube{
    $docker_install =  check_program_installed("Docker Desktop")
    if ( $docker_install-eq "True"){
        New-Item -Path 'c:\' -Name 'minikube' -ItemType Directory -Force
        Invoke-WebRequest -OutFile 'c:\minikube\minikube.exe' -Uri 'https://github.com/kubernetes/minikube/releases/latest/download/minikube-windows-amd64.exe' -UseBasicParsing
        $oldPath = [Environment]::GetEnvironmentVariable('Path', [EnvironmentVariableTarget]::Machine)
        if ($oldPath.Split(';') -inotcontains 'C:\minikube'){ `
          [Environment]::SetEnvironmentVariable('Path', $('{0};C:\minikube' -f $oldPath), [EnvironmentVariableTarget]::Machine) `
        }
        Write-ColorOutput "Zainstalowano minikube"  Green
    }
}

function install_maven($default_software_versions){
    $version_maven = 0
    while($version_maven -eq 0){
        $version_maven = Read-Host "Podaj wersje maven: "
        if (!$version_maven){
            Write-ColorOutput "Nie wprowadzono zadnej wersji maven wiec zostanie wybrana wersja z pliku json"  'Yellow'
            $version_maven =  return_default_software_version $default_software_versions "maven"
        }
    }
    Write-ColorOutput "Wybrano  wersje: $version_maven"
    $url="https://dlcdn.apache.org/maven/maven-3/$version_maven/binaries/apache-maven-$version_maven-bin.zip"
    $zipPath= "C:/tmp/apache-maven-$version_maven-bin.zip"
    $apachePath = "${env:ProgramFiles(x86)}\Apache"
    $mavenPath = "$apachePath\Maven"
    $apache_maven_path = "C:\apache-maven\apache-maven-$version_maven"
    if(Test-Path $mavenPath){
        Remove-Item $mavenPath -Recurse -Force
    }
    
    if(-not (Test-Path $apachePath)) {
        New-Item $apachePath -ItemType directory -Force
    }
    try{
        [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls13
        (New-Object Net.WebClient).DownloadFile($url, $zipPath)
        Write-ColorOutput "Trwa rozpakowywanie archiwum apache-maven-$version_maven-bin.zip"  Blue
        expand-archive -path $zipPath -destinationpath  C:\apache-maven | Out-Null
        Write-ColorOutput "rozpakowano archiwum apache-maven-$version_maven-bin.zip"  Blue
        [IO.Directory]::Move($apache_maven_path, $mavenPath)
        Write-ColorOutput "przeniesiono katalog"  Blue
        Remove-Item 'C:\apache-maven' -Recurse -Force
        Write-ColorOutput "usunieto katalog C:\apache-maven "  Blue
        Remove-Item $zipPath
        Write-ColorOutput "usunieto plik $zipPath"   Blue
        [Environment]::SetEnvironmentVariable("M2_HOME", $mavenPath, "Machine")
        Write-ColorOutput "Ustawionio zmienna srodowiskowa M2_HOME"   Green
        [Environment]::SetEnvironmentVariable("M2", "$mavenPath\bin", "Machine")
        Write-ColorOutput "Ustawionio zmienna srodowiskowa M2"   Green
        [Environment]::SetEnvironmentVariable("PATH", "$mavenPath\bin"+";", "Machine")
        Write-ColorOutput "Ustawionio zmienna srodowiskowa  PATH"  'Green'
        Write-ColorOutput "Apache Maven $version_maven zostal zainstalowany"  'Green'
    }catch{
       Write-ColorOutput "nie udalo sie pobrac danej wersji maven"   'Red'
    }
}

function install_wsl(){
    configuration_Hyper_V
    $ErrorActionPreference = 'SilentlyContinue'
    Write-Output "Trwa instalacja wsl" 
    $services=('/featurename:Microsoft-Windows-Subsystem-Linux', '/featurename:VirtualMachinePlatform')
    foreach($service in $services){
        $service_exists = Get-Service -Name $service -ErrorAction SilentlyContinue; 
        if($service_exists) {
            dism.exe /online /enable-feature $service /all /norestart
            Write-ColorOutput "Enable $service feature"   'Green'
        }
    }
    wsl --set-default-version 2
    Write-ColorOutput "WSL package is installed"  'Blue'
    wsl --install -d Ubuntu
    $distributions=[ordered]@{"Ubuntu.appx"="https://aka.ms/wslubuntu2204"}
    $ProgressPreference='Silent' 
    foreach($package in $distributions.Keys){
        Write-ColorOutput "Downloading $package package ...."  'Blue'
        try {
            $variable = Invoke-WebRequest -Uri $distributions[$package] -OutFile $package
            Write-ColorOutput "Successfully Downloaded $package package"  'Green'
        }
        catch {
            Write-ColorOutput "Failed to downloaded $package package"  'Red'
        }
        Add-AppxPackage $package
        Write-ColorOutput "The $package has been installed successfully"  'Green' 
        Remove-Item -path $package
        Write-ColorOutput "Deleted installation package $package file"  'Green' 
    } 
}

function webrowsers_install(){
    $workdir = "c:\tmp\"
    $webrowsers=@{"Mozilla Firefox (x64 pl)" = "https://download.mozilla.org/?product=firefox-latest&os=win64&lang=en-US";
    "Google Chrome" = "http://dl.google.com/chrome/install/latest/chrome_installer.exe"}
    foreach ($key in $workdir.Keys){
        $webrowser_install = check_program_installed("$key")
        if( $webrowser_install -eq "False"){
            try{
                Write-ColorOutput "Rozpoczeto sciaganie  ${key}.exe"
                Invoke-WebRequest  -Uri $webrowsers[$key] -OutFile "${workdir}${key}.exe" -UseBasicParsing
                Write-ColorOutput "Pobrano ${key}.exe"
                Start-Process -FilePath  "${workdir}${key}.exe" -Args "/silent /install" -Verb RunAs -Wait
                Write-ColorOutput "zakonczono instalacje $key"  Green
            }
            catch{
                Write-ColorOutput "nie udalo sie wgrac $key"   Red
            }
        }
    }   
}

function signal_install(){
    [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls13
    Invoke-WebRequest  -Uri "https://updates.signal.org/desktop/signal-desktop-win-${SIGNAL_VERSION}.exe"  -OutFile "C:/tmp/signal-desktop-win-${SIGNAL_VERSION}.exe" -UseBasicParsing
    Start-Process -FilePath  "C:/tmp/signal-desktop-win-${SIGNAL_VERSION}.exe" -Args "/silent /install" -Verb RunAs -Wait
    Write-ColorOutput "Instalacja Signal"
}

function ghidra_install(){
    $ghidra_directory='C:/ghidra'
    New-Item -Path  "${ghidra_directory}" -Type Directory
    $ghidra_zip=[System.IO.Path]::GetFileName("$GHIDRA_LINK_DOWLOAND")
    Write-ColorOutput "Instalacja GHIDRA"
    Invoke-WebRequest  -Uri "${GHIDRA_LINK_DOWLOAND}" -OutFile "C:/tmp/$ghidra_zip" -UseBasicParsing
    New-Item -Path  "${ghidra_directory}" -Type Directory
    [System.IO.Compression.ZipFile]::ExtractToDirectory("C:/tmp/$ghidra_zip", "${ghidra_directory}")
}

function develop_setup(){
    $softwares_dev = @{'Virtualbox'='Oracle.VirtualBox'; 'Vagrant'='Hashicorp.Vagrant'; 'DBeaver'='dbeaver.dbeaver'; 
    'VisualStudioCode'='Microsoft.VisualStudioCode'; 'Git'='Git.Git'; 'Intellij Community'='JetBrains.IntelliJIDEA.Community';
    'Windows Terminal'='Microsoft.WindowsTerminal'; 'Keepass'='DominikReichl.KeePass';  'trippy'='trippy'; 'IDA.Free'='Hex-Rays.IDA.Free'; 
    'OpenJDK17'='Microsoft.OpenJDK.17'; 'Rustlang Rustup' = 'Rustlang.Rustup'; 'Golang'= 'GoLang.Go.1.19'; 'k9s'= 'k9s'}
    softwares_install($softwares_dev)
    $default_software_versions = read_json_file('./main_dependencies/json_files/default_software_versions.json')
    install_python($default_software_versions)
    install_maven($default_software_versions)
    install_wsl
    install_minikube
}

function no_develop_setup(){
    $softwares = @{'CCleaner'='Piriform.CCleaner'; 'Vlc'='VideoLAN.VLC'; 'LibreOffice'='TheDocumentFoundation.LibreOffice';
    'Adobe.Acrobat.Reader'='Adobe.Acrobat.Reader.64-bit'}
    $url_softwares =@{'steam'='https://cdn.akamai.steamstatic.com/client/installer/SteamSetup.exe';  
    'eset'='https://download.eset.com/com/eset/tools/installers/live_essp/latest/eset_smart_security_premium_live_installer.exe'}
    softwares_install($softwares)
    softwares_url_install($url_softwares)
    install_steam
    install_eset
}

function windows_update(){
    Install-Module PSWindowsUpdate -AcceptLicense -AllowPrerelease -Scope CurrentUser -Force -SkipPublisherCheck 
    Write-ColorOutput "Dostepne aktualizacje systemu" "Blue"
    Get-WindowsUpdate
    Write-ColorOutput "Rozpoczento aktualizacje systemu" "Blue"
    try{
        Install-WindowsUpdate 
        Write-ColorOutput "Pobrano najnowsze aktualizacje" "Green" 
    }
    catch{
        Write-ColorOutput "Nie udało sie pobrać aktualizaji" "Red"
    }

}

function install_software_github(){
   $github_softwares=@{"lazydocker"="https://raw.githubusercontent.com/jesseduffield/lazydocker/master/scripts/install_update_linux.sh";
   "kustomize"="https://raw.githubusercontent.com/kubernetes-sigs/kustomize/master/hack/install_kustomize.sh";
   "act" ="https://raw.githubusercontent.com/nektos/act/master/install.sh"; 
   "tfsec"="https://raw.githubusercontent.com/aquasecurity/tfsec/master/scripts/install_linux.sh"}
    foreach($software in $github_softwares.Keys){
        curl -s "${github_softwares[$software]}";
    }
    $github_softwares_curl=@{'container-structure-test'="https://storage.googleapis.com/container-structure-test/latest/container-structure-test-linux-amd64";
    'hadolint' ="https://github.com/hadolint/hadolint/releases/download/v${HADOLINT_VERSION}/hadolint-Linux-x86_64";
    'skaffold' =' https://storage.googleapis.com/skaffold/releases/latest/skaffold-linux-amd64'
    }
    foreach($software in $github_softwares_curl.Keys){
        curl -L "${github_softwares_curl[$software]}" -o "${github_softwares_curl[$software]##*/}"
    }
}

function start($setup){
    Write-ColorOutput "Skrypt rozpoczyna dzialanie"  "Blue"
    create_tmp_directory
    Write-ColorOutput "$setup"
    if ($setup -eq "dev") {
        Write-ColorOutput "Odpalono setup dla developera" "Blue"
        no_develop_setup
        develop_setup
    }
    else {
        Write-ColorOutput "Odplano setup dla zwykłego user" "Blue"
        no_develop_setup
    }
    webrowsers_install
    delete_tmp_directory
    windows_update
    Write-ColorOutput "Wszystko wykonalo sie pomyslnie"  "Green"
    Write-ColorOutput "Nastapi teraz restart komputera"  "Yellow"
}

#%systemdrive%\Windows\Temp

function main($setup){
    $GRADLE_VERSION=(Invoke-RestMethod -Uri  "https://api.github.com/repos/gradle/gradle/releases/latest" | Select-object -ExpandProperty tag_name ).Replace('v','')
    $GHIDRA_VERSION=((Invoke-RestMethod -Uri  "https://api.github.com/repos/NationalSecurityAgency/ghidra/releases/latest"  | Select-object  -ExpandProperty name ).split(' '))[1]
    $GHIDRA_LINK_DOWLOAND=(Invoke-RestMethod -Uri  "https://api.github.com/repos/NationalSecurityAgency/ghidra/releases/latest" | Select-object  -ExpandProperty assets | Select-Object -ExpandProperty browser_download_url )
    $SIGNAL_VERSION=(Invoke-RestMethod -Uri  "https://api.github.com/repos/signalapp/Signal-Desktop/releases/latest"  | Select-object  -ExpandProperty tag_name ).Replace('v','')
    $HADOLINT_VERSION=$(curl -s "https://api.github.com/repos/hadolint/hadolint/releases/latest"  | grep -Po '"tag_name": "v\K[0-9.]+')
    if($IsWindows){
        Write-ColorOutput "Wykryto  system  windows"  "Magenta"
        Write-ColorOutput "skrypt jest uruchamiany na wersji systemu $($PSVersionTable.OS)"  "Magenta"
        check_version
        $currentPrincipal = New-Object Security.Principal.WindowsPrincipal([Security.Principal.WindowsIdentity]::GetCurrent())
        if($currentPrincipal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)){
            Write-ColorOutput "Masz uprawnienia administratora"  "green"
            start $setup
        }
        else{
            Write-Error "nie masz uprawnien administratora uruchom skrypt na prawach administratora"
        }
    }
    elseif($IsMacOS){
        Write-Warning "Wykryto system MacOS" 
        write-Error  "Nie mozna uruchomic skryptu na tym systemie operacyjnym" 
    }
    elseif($IsLinux){
        Write-Warning "Wykryto system Linux"
        Write-Error  "Nie mozna uruchomic skryptu na tym systemie operacyjnym"
    }
}

main "$($args[0])"
