Set-Variable -Name PathModules -Value "../../main_dependencies/PSModule" -Option Constant
$modules=('InstallVagrant/InstallVagrant.psd1', 'UpdatePowershell/UpdatePowershell.psd1', 'Functions/Functions.psd1')
foreach($module in $modules){
    if(Test-Path "$PathModules/$module"){
        Import-Module -Name "$PathModules/$module"
    }
    else{
        Write-Error "Brak modulu -> $module"
        Exit 1
    } 
}

function menu($menu){
	clear
	if($menu -eq 'start'){
		Write-ColorOutput "1) Run virtual machines" -ForegroundColor 'Blue'
		Write-ColorOutput "2) Remove virtual machines" -ForegroundColor 'Blue'
		Write-ColorOutput "3) QUIT" -ForegroundColor 'Blue'
	}
	elseif($menu -eq 'run'){
		Write-ColorOutput "1) Uruchom wybrane maszyne zdefiniowane w Vagrantfile" -ForegroundColor 'Blue'
		Write-ColorOutput "2) Uruchom wszystkie maszyny zdefiniowane w Vagrantfile"  -ForegroundColor 'Blue'
		Write-ColorOutput "3) Wroc" -ForegroundColor 'Blue'
	}
	else{
		Write-ColorOutput "1) Usun wybrane maszyne zdefiniowane w Vagrantfile" -ForegroundColor 'Blue'
		Write-ColorOutput "2) Usun wszystkie maszyny zdefiniowane w Vagrantfile"  -ForegroundColor 'Blue'
		Write-ColorOutput "3) Wroc" -ForegroundColor 'Blue'
	}
}
function return_machine($content_json){
	$machines= [System.Collections.ArrayList]@()
    if(!$null -eq $content_json){
        foreach( $machine in $machines){ $machines.Add($machine) }
    }
    else{
        return 0;
    }
	return $machines
}

function run_machines($machines){
	do{
		menu('run')
		$option= Read-Host -Prompt "Select option: "
		switch($option){
			1 {
				Write-ColorOutput "Wybrales opcje $option " -ForegroundColor 'Cyan'
				Write-ColorOutput "Lista dostepnych maszyn zdefiniowanych w pliku Vagrantfile" -ForegroundColor 'Yellow'
				foreach($name_machine in $machines){
					Write-Output "$name_machine"
				}
				$user_machine = Read-Host -Prompt "Jaki maszyne wirtualna chcesz postawic: "
				foreach($name_machine in $machines){
					if ( "$user_machine"  -eq "$name_machine"){c:\HashiCorp\Vagrant\bin\vagrant.exe up "$user_machine" ; break}
					else{Write-ColorOutput "Nie zdefiniowano w plikach Vagrantfile: $user_machine" -ForegroundColor 'Yellow'}                 
				}
				menu('run')
			}
			2 {
				Write-ColorOutput "Wybrales opcje $option " -ForegroundColor 'Cyan'
				Write-ColorOutput "Stawianie maszyn wirtualnych zawartych w pliku Vagrantfile" -ForegroundColor 'Green'
				c:\HashiCorp\Vagrant\bin\vagrant.exe up;
				menu('run')
			}
			3 {Write-ColorOutput "Wybrales opcje $option " -ForegroundColor 'Cyan'; break}
			default { Write-ColorOutput "Invalid option: $option" -ForegroundColor 'Yellow'}
		}
	}while( "$option" -ne '3' )
}

function delete_machines(){3
	do{
		menu('delete')
		$option= Read-Host -Prompt "Select option: "
		switch($option){
			1 {
				Write-ColorOutput "Wybrales opcje $option" -ForegroundColor 'Cyan'; c:\HashiCorp\Vagrant\bin\vagrant.exe destroy
			}
			2 {
				Write-ColorOutput "Wybrales opcje $option" -ForegroundColor 'Cyan'; c:\HashiCorp\Vagrant\bin\vagrant.exe destroy -f
			}
			3 {Write-ColorOutput "Wybrales opcje $option " -ForegroundColor 'Cyan'; break}
			default {Write-ColorOutput "Invalid option: $option" -ForegroundColor 'Yellow'}
		}
	}while("$option" -ne '3')
}

function main_function($machines){
	do{
		menu('start')
		$option= Read-Host -Prompt "Select option: "
		switch($option){
			1 {run_machines($machines)}
			2 {delete_machines}
			3 {Write-ColorOutput "Exit"; break}
			default {Write-ColorOutput "Invalid option: $option" -ForegroundColor 'Yellow'}
		}
	}while("$option" -ne '3')
}

function main(){
	check_version
	winget_softwares(@{'Virtualbox'='Oracle.VirtualBox'})
	vagrant_install
	$content_json = read_json_file('../../main_dependencies/json_files/vagrant_machines.json')
	$machines = return_machine($content_json)
	main_function($machines)
}
main