# Skrypt do setup środowiska dla systemów Windows

## 1)Opisz głównych skryptów

    vagrant_start.ps1 - skrypt do zarzadzanie maszynami do testowania skryptów

## 2)Uruchamianie skryptow

* ./vagrant_start.ps1

## 3) Uwagi

* <https://superuser.com/questions/106360/how-to-enable-execution-of-powershell-scripts> (Trzeba ustawiać w powershell polityki ponieważ domyślne jest zablokowane uruchamianie skryptów powershell w Windows)
