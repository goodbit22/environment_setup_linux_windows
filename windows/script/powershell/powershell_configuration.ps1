Set-Variable -Name PathModules -Value "../../main_dependencies/PSModule" -Option Constant
$modules=('UpdatePowershell/UpdatePowershell.psd1')
foreach($module in $modules){
    if(Test-Path "$PathModules/$module"){
        Import-Module -Name "$PathModules/$module"
    }
    else{
        Write-Error "Brak modulu -> $module"
        Exit 1
    } 
}

function fonts_configuration($fonts_path='c:/Windows/Fonts'){
    $fonts = ("Meslo" , "Hack", "3270")
    foreach($font in $fonts){
        if ( !(Test-Path "$fonts_path/$font*") ){
            try{
                oh-my-posh font install "$font"
                Write-ColorOutput "Zainstalowano czcionke -> $font" -ForegroundColor 'Green'
            }
            catch{
                Write-Error "Nie udalo Zainstalować czcionki -> $font"
                Write-ColorOutput "Uruchom ponownie skrypt w nowej sesji terminala (otworz nowe okienko powershell7) "  -ForegroundColor 'Yellow'
                Exit 1
            }
        }
    }
}
function profile_configuration(){
    if ( !(Test-Path $Profile) ){
        New-Item -Path $PROFILE -Type File -Force
    }
    fonts_configuration
    $entries=("& ([ScriptBlock]::Create((oh-my-posh init pwsh --config $env:POSH_THEMES_PATH\iterm2.omp.json --print) -join '`n'))", "Import-Module -Name Terminal-Icons", "Import-Module -Name PSReadLine",
    "Set-PSReadLineOption -PredictionSource History", "Set-PSReadLineOption -PredictionViewStyle ListView", "Set-PSReadLineOption -EditMode Windows")
    foreach ($entry in  $entries){
        if ( !(Get-Content -Path "$Profile" | Select-String -SimpleMatch "$entry") ){
            if (!($null  -eq $env:POSH_THEMES_PATH)){
                "$entry" | Out-File -FilePath $PROFILE -Append
                Write-ColorOutput "Wprowadzono wpis -> $entry " -ForegroundColor 'Green'
            }
        }
    }
    [string]$old_string='// Put settings here that you want to apply to all profiles.'
    [string]$new_string='"font": { "face": "MesloLGM NF" }'
    $current_path=$PWD
    $path_windows_terminal = "C:\users\$env:UserName\AppData\Local\Packages\Microsoft.WindowsTerminal_*\LocalState"
    try {
        Set-Location $path_windows_terminal
        (Get-content settings.json)  -replace $old_string, $new_string | Set-content settings.json
        Write-ColorOutput "Ustawiono konfiguracje czcionki -> $new_string " -ForegroundColor 'Green'
    }
    catch{
        Write-ColorOutput "Uruchom ponownie skrypt w Windows Terminal, aby dokonczyc konfiguracje czcionki"  -ForegroundColor 'Yellow'
    }
    Set-Location $current_path
    . $PROFILE
}

function posh_conf(){
    winget list --id 'JanDeDobbeleer.OhMyPosh' > $null
    if ( $? -eq $false){
        winget install --id 'JanDeDobbeleer.OhMyPosh'
    }
    if ( (Get-PSRepository -Name 'PsGallery' | Select-Object -ExpandProperty InstallationPolicy) -eq 'Untrusted'){
        Set-PSRepository -Name "PSGallery" -InstallationPolicy Trusted
        Write-ColorOutput "Zmieniono InstallationPolicy dla repo PSGallery z Untrusted -> Trusted" -ForegroundColor 'Green'
    } 
    if (!(Get-Module -ListAvailable -Name Terminal-Icons )) {
        Install-Module -Name Terminal-Icons  -AcceptLicense
    }
    Install-Module -Name PSReadLine -AllowPrerelease -Scope CurrentUser -Force -SkipPublisherCheck  -AcceptLicense
    profile_configuration
}
check_version
posh_conf