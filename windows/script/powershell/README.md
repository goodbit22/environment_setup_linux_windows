# Skrypt do setup środowiska dla systemów Windows

    powershell_configuration.ps1  - skrypt do konfiguracji powłoki powershell

## 2)Uruchamianie skryptow

* .\powershell_configuration.ps1

## 3) Uwagi

* <https://superuser.com/questions/106360/how-to-enable-execution-of-powershell-scripts> (Trzeba ustawiać w powershell polityki ponieważ domyślne jest zablokowane uruchamianie skryptów powershell w Windows)
