# Skrypt do setup środowiska dla systemów Windows

    docker_images.ps1 - skrypt do instalacji obrazow dockerowych

## 2)Uruchamianie skryptow

* .\docker_images.ps1

## 3) Uwagi

* <https://superuser.com/questions/106360/how-to-enable-execution-of-powershell-scripts> (Trzeba ustawiać w powershell polityki ponieważ domyślne jest zablokowane uruchamianie skryptów powershell w Windows)
