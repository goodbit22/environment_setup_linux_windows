Set-Variable -Name PathModules -Value "../../main_dependencies/PSModule" -Option Constant
$modules=('UpdatePowershell/UpdatePowershell.psd1', 'Functions/Functions.psd1')
foreach($module in $modules){
    if(Test-Path "$PathModules/$module"){
        Import-Module -Name "$PathModules/$module"
    }
    else{
        Write-Error "Brak modulu -> $module"
        Exit 1
    } 
}

function read_json_file_with_images_docker($json_file_with_docker_images = '../../../json_files/images_docker.json'){
    if (Test-Path -Path $json_file_with_docker_images){
        $docker_images = Get-Content $json_file_with_docker_images | Out-String | ConvertFrom-Json
    }
    elseif($NULL -eq (Get-Content $json_file_with_docker_images)){
        Write-Warning "plik $json_file_with_docker_images jest pusty"
        $docker_images = $null
    }
    else{
        Write-Error "plik $json_file_with_docker_images  nie istnieje wiec  nie mozna pobrac obrazow dockerowych"
        $docker_images = $null
    }
    return $docker_images;
}

function pull_docker_images(){
    $docker_im =check_program_installed("Docker Desktop")
    if ( $docker_im -eq "False"){
        $docker_images = read_json_file_with_images_docker
        foreach ($docker_image in $docker_images){
            $image = $docker_image.name + ':' + $docker_image.tag
            if("$image" -ne ':') {
                Write-ColorOutput  "rozpoczeto pobieranie obrazu:  $image" -ForegroundColor "blue"
                docker pull $image
            }
        }
    }
    else{
        Write-Error "Nie mozesz sciagnac obrazow "
    }
}
check_version
pull_docker_images