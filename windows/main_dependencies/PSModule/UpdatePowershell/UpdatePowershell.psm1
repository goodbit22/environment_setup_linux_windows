function install_winget(){
    $ErrorActionPreference = 'SilentlyContinue'
    winget -v > $null
    $status = $?
    if ($status -eq $false) {
        Write-Output "Winget package is installed" -ForegroundColor 'Blue'
        $winget_dep=[ordered]@{"Microsoft.VCLibs.x64.14.00.Desktop.appx"="https://aka.ms/Microsoft.VCLibs.x64.14.00.Desktop.appx"
        "Microsoft.DesktopAppInstaller_8wekyb3d8bbwe.msixbundle"="https://github.com/microsoft/winget-cli/releases/download/v1.3.2691/Microsoft.DesktopAppInstaller_8wekyb3d8bbwe.msixbundle"}
        $ProgressPreference='Silent'
        foreach($package in $winget_dep.Keys){
            $variable = Invoke-WebRequest -Uri $winget_dep[$package] -OutFile $package
            Write-Output "Successfully Downloaded $package package" -ForegroundColor 'Green'
            Add-AppxPackage $package
            Write-Output "The $package has been installed successfully" -ForegroundColor 'Green'
            Remove-Item -path $package
            Write-Output "Deleted installation package $package file" -ForegroundColor 'Green'
        }
    }
    else {
        Write-Output "Winget package was installed early" -ForegroundColor 'Green'
    }
}

function install_powershell7(){
    $ErrorActionPreference = 'SilentlyContinue'
    winget list  --accept-source-agreements -q 'Powershell 7-x64' > $null
    if ( $? -eq $false){
        $url = 'https://github.com/PowerShell/PowerShell/releases/download/v7.2.8/PowerShell-7.2.8-win-x64.msi'
        $path_file_msi= "C:PowerShell-7.2.8-win-x64.msi"
        Write-Output "Powershell 7 download in progress"  -ForegroundColor 'Blue'
        try{
            Invoke-WebRequest -OutFile $path_file_msi  -Uri $url -UseBasicParsing
            Write-Output "Successfully downloaded the update" -ForegroundColor 'Green'
            Start-Process $path_file_msi -ArgumentList "/passive /norestart" -wait
            Write-Output "Udalo sie zainstalowac aktualizacje" -ForegroundColor 'Green'
            if (Test-Path $path_file_msi){
                Remove-Item -path $path_file_msi
                Write-Output "Deleted downloaded file"  -ForegroundColor 'Green' 'Remove dowloanded file'
            }
        }
        catch{
            Write-Error "powershell update failed to download"
        }
       Write-Output "Checking for updates"; winget upgrade -q Microsoft.PowerShell
    }
    else {
        Write-Output "Powershell7 was installed early"  -ForegroundColor 'Blue'
    }
}
function check_version(){
    $major_version_powershell = [int]$PSVersionTable.PSVersion.Major
    if ("$major_version_powershell" -lt 7){
        Write-Warning "The version of  powershell is lower than 7" 
        install_winget
        install_powershell7
        Write-Warning "Run the script in Powershell7"; Exit
    }
    else {
        Write-Output "Running the script in Powershell7"  -ForegroundColor 'Blue'
    }
}
