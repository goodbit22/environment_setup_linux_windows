$modules=('InstallVagrant', 'UpdatePowershell', 'Functions')
foreach($module in $modules){
    if( $null -ne (Get-Module -Name "$module" | Select-Object -ExpandProperty Name) ){
        Remove-Module -Name "$module"
        Write-Output "Usunieto moduł: $module"
    }
}