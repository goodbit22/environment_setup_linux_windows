function Write-ColorOutput
{
    [CmdletBinding()]
    Param(
         [Parameter(Mandatory=$False,Position=1,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)][Object] $Object,
         [Parameter(Mandatory=$False,Position=2,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)][ConsoleColor] $ForegroundColor,
         [Parameter(Mandatory=$False,Position=3,ValueFromPipeline=$True,ValueFromPipelinebyPropertyName=$True)][ConsoleColor] $BackgroundColor,
         [Switch]$NoNewline
    )    

    # Save previous colors
    $previousForegroundColor = $host.UI.RawUI.ForegroundColor
    $previousBackgroundColor = $host.UI.RawUI.BackgroundColor

    # Set BackgroundColor if available
    if($BackgroundColor -ne $null)
    { 
       $host.UI.RawUI.BackgroundColor = $BackgroundColor
    }

    # Set $ForegroundColor if available
    if($ForegroundColor -ne $null)
    {
        $host.UI.RawUI.ForegroundColor = $ForegroundColor
    }

    # Always write (if we want just a NewLine)
    if($null -eq $Object)
    {
        $Object = ""
    }

    if($NoNewline)
    {
        [Console]::Write($Object)
    }
    else
    {
        Write-Output $Object
    }

    # Restore previous colors
    $host.UI.RawUI.ForegroundColor = $previousForegroundColor
    $host.UI.RawUI.BackgroundColor = $previousBackgroundColor
}

function check_registry_entries{
    param (
        [string] $register_key,
        [string] $property
    )
    $ErrorActionPreference = 'SilentlyContinue'
    $test=Get-ItemProperty -Path Registry::"$register_key" | Select-object -ExpandProperty "$property"; $status="$?"
    if ($status -eq $false){ return $false }; return $true
}

function check_directory_exists{
    param([string] $path)
    if(Test-Path $path){
        return 1
    }
    else {
        return 0
    }
}

function create_tmp_directory($path="C:", $name_tmp_directory="tmp"){
    $path="$path\$name_tmp_directory"
    if (Test-Path $path){
        Write-Output "The $name_tmp_directory directory already exists"
    }
    else{
        New-Item -Path $path  -ItemType Directory
        Write-Output "The $name_tmp_directory directory has been created"
    } 
}

function delete_tmp_directory($path="C:", $name_tmp_directory="tmp"){
    $path="$path\$name_tmp_directory"
    if (Test-Path $path){
        Remove-Item –path $path –recurse
        Write-Information "The $name_tmp_directory directory has been deleted"
    }
    else{
        Write-Error "The $name_tmp_directory directory is not exists"
    }
}

function check_program_installed{
    param([string] $software)
    $installed = $null -ne  (Get-ItemProperty HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\* | Where-Object { $_.DisplayName -eq $software })
    If(-Not $installed) {
        Write-Information "$software is not already installed."
        return "False";
    } else {
        Write-Information "'$software'is already installed."
        return "True";
    }
}
function softwares_install($softwares){
    foreach($software in $softwares.Keys){
        Write-Progress "Checking whether the $software program was previously installed is in progress"
        $software_install = check_program_installed("$software")
        if($software_install  -eq "False"){
            try{
                Write-Output "Installations have started: $software"
                winget install --accept-package-agreements  --accept-source-agreements -e --id  $softwares[$software]
                Write-Output "Installed: $software"
            }
            catch{
                Write-Error "Failed to install: $software"
            }
        }
        else{ Write-Warning "already this program $software is installed"}
    }
}

function softwares_url_install($url_softwares){
    foreach($url_software in $url_softwares.Keys){
        $file_exe = $url_softwares[$url_software].Substring($url_softwares[$url_software].LastIndexOf('/') + 1)
        $path_file_exe =  "C:\tmp\$file_exe"
        if(Test-Path  $path_file_exe){
            Write-Warning "Pobrano wczesiej $file_exe"
        }
        else{
            $soft_install =  check_program_installed($url_software)
            if($soft_install -eq 'False'){
                Write-Output "Zaczeto sciaganie pliku "
                [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls13
                try{
                    Invoke-WebRequest -Uri $url_softwares[$url_software] -OutFile "$path_file_exe"
                    Write-Information "Pobrano plik $file_exe i rozpoczeto jego instalacje"
                    Start-Process  "$path_file_exe"    -argumentlist "/passive /norestart"
                    Write-Output "zakonczono instalacje $url_software"
                }
                catch{
                    Write-Error "Problem z instalacja $url_software"
                }
            }
            else{
                Write-Warning "Juz jest wgrany ${url_softwares[$url_software]}"
            }

        }
    }
}

function winget_softwares($softwares){
    foreach($software in $softwares.Keys){
        winget list --id $softwares[$software] > $null
        if ( $? -eq $false){
            winget install --id $softwares[$software]
            Write-Information "Installed $software"
        }
    }
}

function read_json_file(){
    param(
        [string]$path_json
    )
    if (Test-Path -Path $path_json){
        $json_content = Get-Content $path_json | Out-String | ConvertFrom-Json
    }
    else{
        Write-Error "The $path_json file does not exists"
        $json_content = $null
    }
    return $json_content;
}

