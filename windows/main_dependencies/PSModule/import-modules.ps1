
$modules=('Functions/Functions.psd1', './InstallVagrant/InstallVagrant.psd1', './UpdatePowershell/UpdatePowershell.psd1')
foreach($module in $modules){
    if(Test-Path "$PathModules2/$module"){
        Import-Module -Name $PathModules2/$module
        Write-ColorOutput "Zaimportowano moduł: $module" "Green"
    }
    else{
        Write-Error "Brak modulu -> $module"
        Exit 1
    }
}
