function vagrant_install{
	param(
		$vagrant_plugins = ("vagrant-vbguest")
	)
	winget list --id 'Hashicorp.Vagrant' > $null
    if ( $? -eq $false){
        winget install -e --id 'Hashicorp.Vagrant'
		Write-Output "Installed Vagrant" -ForegroundColor 'Green'
    }
	$list_plugin_ins_vag = @(c:\HashiCorp\Vagrant\bin\vagrant.exe plugin list | Select-Object Name)
	foreach ($plugin in $vagrant_plugins){
        $plugin_ins=0
		foreach ($plug in  "$list_plugin_ins_vag"){
			if ($plugin -eq "$plug" ){
				$plugin_ins=1
				break
			}
		}
		if ("$plugin_ins" -eq 0 ){
			c:\HashiCorp\Vagrant\bin\vagrant.exe plugin install "$plugin" > $null
			Write-Output "zainstalowano plugin $plugin" -ForegroundColor 'Green'
		}
    }
}